const mongoose = require('mongoose');
const { Schema } = mongoose;

const TemporaryAuthenticationSchema = new Schema({
  person: { type: Schema.Types.ObjectId, ref: 'Person' }, //the _id record associated with a found record.
  createdAt: {
    type: Date,
    default: new Date()
  }
});

TemporaryAuthenticationSchema.index({ createdAt: 1 }, { expires: 60 * 60 });

const TemporaryAuthentication = mongoose.model(
  'TemporaryAuthentication',
  TemporaryAuthenticationSchema
);

// make this available to our users in our Node applications
module.exports = TemporaryAuthentication;
