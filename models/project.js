const mongoose = require('mongoose');
const { Schema } = mongoose;

const CustomFieldSchema = require('./customFieldSchema');
const FileSchema = require('./fileSchema');
const ProjectAssignmentSchema = require('./projectAssignmentSchema');

const ProjectSchema = new mongoose.Schema(
  {
    active: { type: Boolean, default: true },
    tenant: { type: Schema.Types.ObjectId, ref: 'Tenant' },
    id: { type: String, required: true },
    name: { type: String, required: true },
    address: String,
    latitude: Number,
    longitude: Number,
    customFields: CustomFieldSchema,
    files: [FileSchema],
    personAssignments: [ProjectAssignmentSchema]
  },
  {
    timestamps: true,
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
  }
);

ProjectSchema.index({ tenant: 1, id: 1 }, { unique: true }); // insure unique ids for tenants

var Project = mongoose.model('Project', ProjectSchema);
module.exports = Project;
