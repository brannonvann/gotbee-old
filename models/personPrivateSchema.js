const { Schema } = require('mongoose');
const MAX_LOGIN_ATTEMPTS = parseInt(process.env.MAX_LOGIN_ATTEMPTS); //maximum number of attempts allowd to login before account is locked.

//Private property should be removed before person is returned for any api or page load.
const PersonPrivateSchema = new Schema(
  {
    password: { type: String },
    loginAttempts: { type: Number, required: true, default: 0 },
    lockUntil: { type: Number }
  },
  {
    _id: false,
    toObject: { virtuals: true }
  }
);

/** Virtual Properties */

/** virtual property whether or not the account is locked using the lockUntil property. */
PersonPrivateSchema.virtual('isLocked').get(function() {
  // check for a future lockUntil timestamp

  return this.lockUntil ? this.lockUntil > Date.now() : false;
});

module.exports = PersonPrivateSchema;
