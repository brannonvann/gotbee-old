const mongoose = require('mongoose');
const { Schema } = mongoose;

const CustomFieldSchema = new mongoose.Schema({
  name: { type: String },
  kind: {
    type: String,
    enum: ['line', 'multiline', 'email', 'number', 'select', 'checkbox', 'date']
  },
  disabled: Boolean,
  options: [String],
  multiple: Boolean,
  defaultValue: Schema.Types.Mixed,
  required: Boolean
});

const RecordItemSchema = new mongoose.Schema({
  standardField: String,
  customField: { type: Schema.Types.ObjectId, ref: 'CustomFieldSchema' },
  tool: String,
  section: String
});

const FieldColumnSchema = new mongoose.Schema({
  width: {
    type: String,
    enum: ['half', 'full']
  },
  items: [RecordItemSchema]
});

const FieldGroupSchema = new mongoose.Schema({
  name: String,
  columns: [FieldColumnSchema]
});

const ViewSchema = new mongoose.Schema({
  name: { type: String, default: 'Default View' },
  layout: [FieldColumnSchema],
  groups: [FieldGroupSchema]
});

const PeopleConfigSchema = new mongoose.Schema(
  {
    customFields: [CustomFieldSchema],
    views: [ViewSchema]
  },
  { _id: false }
);

const ProjectsConfigSchema = new mongoose.Schema(
  {
    customFields: [CustomFieldSchema],
    views: [ViewSchema]
  },
  { _id: false }
);

const TenantSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, index: { unique: true } },
    peopleConfig: PeopleConfigSchema,
    projectsConfig: ProjectsConfigSchema,
    dateFormat: { type: String, default: 'MMMM D YYYY' }
  },
  {
    timestamps: true
  }
);

var Tenant = mongoose.model('Tenant', TenantSchema);

module.exports = Tenant;
