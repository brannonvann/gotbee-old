const { Schema } = require('mongoose');

const PersonRatingSchema = new Schema({
  overallRating: String,
  occurred: Date,
  notes: String
});

PersonRatingSchema.pre('save', function(next) {
  //same tenant validation

  next();
});

module.exports = PersonRatingSchema;
