const { Schema } = require('mongoose');

const ProjectAssignmentSchema = new Schema({
  person: { type: Schema.Types.ObjectId, ref: 'Person' },
  start: Date,
  end: Date
});

ProjectAssignmentSchema.pre('save', function(next) {
  //same tenant validation

  next();
});

module.exports = ProjectAssignmentSchema;
