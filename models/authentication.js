const mongoose = require('mongoose');
const { Schema } = mongoose;

const AuthenticationSchema = new Schema({
  id: { type: String, required: true }, //the id, access code, or api key used.
  person: { type: Schema.Types.ObjectId, ref: 'Person' }, //the _id record associated with a found record.
  tenant: { type: Schema.Types.ObjectId, ref: 'Tenant' },
  createdAt: {
    type: Date,
    default: new Date(),
    expires: 60 * 60 * 24 * 120 //expires after 120 days
  }, //Date stamp for when the Authentication was attempted.
  source: { type: String, required: true, enum: ['Access Code', 'UI', 'API'] },
  result: { type: String, required: true } //the string result of the authentication attempt.
});

const Authentication = mongoose.model('Authentication', AuthenticationSchema);

// make this available to our users in our Node applications
module.exports = Authentication;
