const { Schema } = require('mongoose');

const FileSchema = new Schema(
  {
    name: String,
    size: Number
  },
  {
    timestamps: true
  }
);

module.exports = FileSchema;
