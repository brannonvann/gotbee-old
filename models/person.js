const mongoose = require('mongoose');
const { Schema } = mongoose;
const bcrypt = require('bcrypt');

const MAX_LOGIN_ATTEMPTS = parseInt(process.env.MAX_LOGIN_ATTEMPTS); //maximum number of attempts allowd to login before account is locked.
const FAILED_LOGIN_LOCK_TIME = parseInt(process.env.FAILED_LOGIN_LOCK_TIME); //the amount of time the account is locked if max failed attempts is reached.

const PersonPrivateSchema = require('./personPrivateSchema');
const CustomFieldSchema = require('./customFieldSchema');
const FileSchema = require('./fileSchema');
const PersonInjurySchema = require('./personInjurySchema');
const PersonRatingSchema = require('./personRatingSchema');

/** Main schema for a person record. */
const PersonSchema = new mongoose.Schema(
  {
    active: { type: Boolean, default: true },
    superAdmin: { type: Boolean },
    admin: { type: Boolean },
    tenant: { type: Schema.Types.ObjectId, ref: 'Tenant' },
    id: { type: String, required: true },
    name: { type: String, required: true },
    email: { type: String, lowercase: true },
    address: String,
    latitude: Number,
    longitude: Number,
    customFields: CustomFieldSchema,
    private: PersonPrivateSchema,
    files: [FileSchema],
    injuries: [PersonInjurySchema],
    ratings: [PersonRatingSchema]
  },
  {
    timestamps: true,
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
  }
);

/** Indicies */

PersonSchema.index({ email: 1 }, { unique: true, sparse: true }); // insure globally unique email addresses, allow nulls
PersonSchema.index({ tenant: 1, id: 1 }, { unique: true }); // insure unique ids for tenants

/** Virtual Properties */

//none//

/** Methods */

/** Method to compare given password with the stored hashed password to see if they are the same. */
PersonSchema.methods.comparePassword = function(candidatePassword, cb) {
  //never allow user without a private entry or without a password to validate.
  if (!this.private || !this.private.password) {
    return cb(null, false);
  }

  bcrypt.compare(candidatePassword, this.private.password, function(err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

/** Method to count loging attempts and block if attempts has been exceeded. */
PersonSchema.methods.incLoginAttempts = function(callback) {
  // if we have a previous lock that has expired, restart at 1
  if (this.private && this.private.lockUntil && this.private.lockUntil < Date.now()) {
    return this.update(
      {
        $set: { 'private.loginAttempts': 1 },
        $unset: { 'private.lockUntil': 1 }
      },
      callback
    );
  }
  // otherwise we're incrementing
  var updates = { $inc: { 'private.loginAttempts': 1 } };
  // lock the account if we've reached max attempts and it's not locked already
  if (
    this.private &&
    this.private.loginAttempts + 1 >= MAX_LOGIN_ATTEMPTS &&
    !this.private.isLocked
  ) {
    updates.$set = { 'private.lockUntil': Date.now() + FAILED_LOGIN_LOCK_TIME };
  }
  return this.update(updates, callback);
};

/** Statics */

/** method to authenticate a user. (email, password, callback) */
PersonSchema.statics.getAuthenticated = function(email, password, cb) {
  this.findOne({ email: email.toLowerCase() }, (err, user) => {
    if (err) return cb(err);

    // make sure the user exists
    if (!user) {
      return cb(null, null, { error: reasons.EMAIL_OR_PASSWORD_INCORRECT });
    }

    // check if the account is currently locked
    if (user.private && user.private.isLocked) {
      // just increment login attempts if account is already locked
      return user.incLoginAttempts(function(err) {
        if (err) return cb(err);
        return cb(null, null, {
          error: reasons.MAX_ATTEMPTS,
          lockedUntil: user.private.lockUntil
        });
      });
    }

    if (user.active !== true) {
      return cb(null, null, { error: reasons.NOT_ALLOWED });
    }

    if (user.admin !== true) {
      //Policy rules
      return cb(null, null, { error: reasons.NOT_ALLOWED });
    }

    // test for a matching password
    user.comparePassword(password, (err, isMatch) => {
      if (err) return cb(err);
      // check if the password was a match
      if (isMatch) {
        // if there's no lock or failed attempts, just return the user
        if (!user.loginAttempts && !user.lockUntil) return cb(null, user);
        // reset attempts and lock info
        var updates = {
          $set: { loginAttempts: 0 },
          $unset: { lockUntil: 1 }
        };

        return user.update(updates, function(err) {
          if (err) return cb(err);
          return cb(null, user);
        });
      }

      // password is incorrect, so increment login attempts before responding
      user.incLoginAttempts(function(err) {
        if (err) return cb(err);
        return cb(null, null, { error: reasons.EMAIL_OR_PASSWORD_INCORRECT });
      });
    });
  }).populate('tenant');
};

// expose enum on the model, and provide an internal convenience reference
const reasons = (PersonSchema.statics.failedLogin = {
  EMAIL_OR_PASSWORD_INCORRECT: 'Incorrect Email Address or Password',
  MAX_ATTEMPTS: 'Max login attempts exceeded.',
  NOT_ALLOWED: 'User does not have permission to login.'
});

var Person = mongoose.model('Person', PersonSchema, 'people');
module.exports = Person;
