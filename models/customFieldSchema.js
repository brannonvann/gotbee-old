const { Schema } = require('mongoose');

const CustomFieldSchema = new Schema(
  {},
  {
    _id: false,
    strict: false
  }
);

module.exports = CustomFieldSchema;
