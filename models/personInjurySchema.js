const { Schema } = require('mongoose');

//Schema for injuries, typically related to worker's comp.
const PersonInjurySchema = new Schema({
  overseeingPerson: { type: Schema.Types.ObjectId, ref: 'Person' },
  occurred: Date,
  notes: String,
  classification: String,
  project: { type: Schema.Types.ObjectId, ref: 'Project' }
});

PersonInjurySchema.pre('save', function(next) {
  //same tenant validation

  next();
});

module.exports = PersonInjurySchema;
