const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const BasicStrategy = require('passport-http').BasicStrategy;
const mongoose = require('mongoose');

const Person = require('../models/person');
const Authentication = require('../models/authentication');

passport.serializeUser((user, done) => {
  //console.log('passport.js --> passport.serializeUser...');
  done(null, user._id);
});

passport.deserializeUser((_id, done) => {
  const projection = {
    name: true,
    id: true,
    admin: true,
    active: true
  };

  Person.findById(_id, projection)
    .populate('tenant')
    .then(user => {
      done(null, user);
    });
});

passport.use(
  new LocalStrategy(
    {
      usernameField: 'email'
    },
    function(email, password, done) {
      // attempt to authenticate user
      Person.getAuthenticated(email, password, function(err, user, reason) {
        if (err) throw err;

        // login was successful if we have a user
        if (user) {
          // handle login success
          //log success login.
          Authentication.create({
            source: 'UI',
            id: email,
            tenant: user.tenant,
            found_id: user._id,
            result: 'Login successful.'
          });
          //return success
          //console.log('SUCCESS -  LOGIN');
          return done(null, user);
        }

        //Log failed login
        Authentication.create({
          source: 'UI',
          id: email,
          result: reason.error ? reason.error : 'Unknown Login Error'
        });

        return done(null, false, reason);
      });
    }
  )
);

passport.use(
  new BasicStrategy(function(email, password, done) {
    // attempt to authenticate user
    Person.getAuthenticated(email, password, function(err, user, reason) {
      if (err) throw err;

      // login was successful if we have a user
      if (user) {
        // handle login success
        //log success login.
        Authentication.create({
          source: 'API',
          id: email,
          tenant: user.tenant,
          found_id: user._id,
          result: 'Login successful.'
        });
        //return success
        //console.log('SUCCESS -  LOGIN');
        return done(null, user);
      }

      //console.log('reason', reason);

      //Log failed login
      Authentication.create({
        source: 'API',
        id: email,
        result: reason.error ? reason.error : 'Unknown Login Error'
      });

      return done(null, false, reason);
    });
  })
);
