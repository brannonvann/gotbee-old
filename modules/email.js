const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

exports.sendEmail = sendEmail;
function sendEmail(to, subject, text, html) {
  const msg = {
    to: to,
    from: 'no-reply@gotbee.com',
    subject: subject,
    text: text,
    html: html
  };
  sgMail.send(msg, function(err, res) {
    if (err) console.log('Error sending mail', err);
  });
}
