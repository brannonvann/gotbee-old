const HttpStatus = require('http-status-codes');

module.exports.apiError = function(code, description) {
  return { code, reason: HttpStatus.getStatusText(code), description };
};
