//application file management module
const aws = require('aws-sdk');
aws.config.region = process.env.AWS_S3_REGION;

const S3_BUCKET = process.env.S3_BUCKET;

module.exports.signedPutUrl = async (Key, ContentType) => {
  const s3 = new aws.S3();

  const params = {
    Bucket: S3_BUCKET,
    Key,
    Expires: 60,
    ContentType,
    ACL: 'private'
  };

  const data = await s3.getSignedUrl('putObject', params);

  return data;
};

module.exports.signedGetUrl = async Key => {
  const s3 = new aws.S3();

  const params = {
    Bucket: S3_BUCKET,
    Key,
    Expires: 60
  };

  const data = await s3.getSignedUrl('getObject', params);

  return data;
};

module.exports.deleteFile = async Key => {
  const s3 = new aws.S3();

  const params = {
    Bucket: S3_BUCKET,
    Key
  };

  return await s3.deleteObject(params).promise();
};
