const { apiError } = require('./utils');

//middleware that validates user is logged in an meets tenant policy
module.exports.allowed = (req, res, next) => {
  //not authenticated
  if (!req.user) {
    return res.status(401).json(apiError(401));
  }

  //authenticated --> validate user meets policy
  if (req.user && req.user.admin === true) {
    return next();
  }

  //not allowed
  res.status(403).json(apiError(403));
};
