const SALT_WORK_FACTOR = parseInt(process.env.SALT_WORK_FACTOR); //salt factor for hashing passwords.
const bcrypt = require('bcrypt');
const generator = require('generate-password');
const email = require('./email');
const Person = require('../models/person');
const TemporaryAuthentication = require('../models/temporaryAuthentication');
const HttpStatus = require('http-status-codes');
const owasp = require('owasp-password-strength-test');
const passport = require('passport');

const CLIENT_DOMAIN = process.env.CLIENT_DOMAIN;

/**
 * Checks whether or not user is logged in as an authenticated user.
 * @param {req} HTTP Request object.
 * @param {res} HTTP Response object.
 * @param {next} next function to run(callback).
 */
module.exports.isLoggedIn = function(req, res, next) {
  if (req.isAuthenticated() && req.user && req.user.active == true) {
    return next();
  }
  //req.session.returnTo = req.originalUrl || req.url;
  //res.status(HttpStatus.UNAUTHORIZED).end();
  //return;
  return passport.authenticate('basic', { session: false })(req, res, next);
};

/**
 * Checks whether or not user is logged in as admin.
 * @param {req} HTTP Request object.
 * @param {res} HTTP Response object.
 * @param {next} next function to run(callback).
 */
module.exports.isLoggedInAdmin = function isLoggedInAdmin(req, res, next) {
  if (
    !req.isAuthenticated ||
    !req.isAuthenticated() ||
    !req.user ||
    req.user.admin !== true ||
    req.user.active !== true
  ) {
    req.session.returnTo = req.originalUrl || req.url;
    if (req.user && !req.user.active === true) {
      return res.render('auth_login', {
        warning: 'Your account was found but has not been activated. Please check back again soon.'
      });
    } else {
      return res.redirect('/login');
    }
  }
  next();
};

module.exports.isSuperAdmin = function isSuperAdmin(req) {
  if (req.user && req.user.superAdmin === true) {
    return true;
  }
  return false;
};

module.exports.isAdmin = function isAdmin(req) {
  if (req.user && req.user.admin === true) {
    return true;
  }
  return false;
};

module.exports.generatePassword = () => {
  return generator.generate({
    length: 16,
    numbers: true,
    symbols: true,
    uppercase: true,
    excludeSimilarCharacters: true,
    strict: true
  });
};

module.exports.forgotPasswordRequest = function resetPassword(emailAddress, callback) {
  const query = {
    email: emailAddress, //email matches.
    active: true //is active
  };

  const projection = { _id: true };

  //lookup person
  Person.findOne(query, projection, (err, person) => {
    if (err) return callback(err);

    if (!person) return callback(new Error('person not found'));

    const data = { person: person._id };

    TemporaryAuthentication.findOneAndRemove(data, (err, result) => {
      if (err) return callback(err, null);

      let tempAuth = new TemporaryAuthentication(data);
      tempAuth.save((err, tempAuth) => {
        if (err) return callback(err, null);

        const resetLink = `${CLIENT_DOMAIN}/fpwr/${tempAuth._id}`;
        const subject = 'Gotbee: Password Reset';
        const text = `A password reset was requested for your Gotbee account.`;
        'Please click the link below to change the password. \n \n ' +
          `${resetLink} \n \n` +
          'This link will expire 1 hour after it was requested.';
        (' If you did not request a password reset, please contact your administrator and inform them of this notification.');

        const html =
          '<p>Hi,</p>' +
          `<p>A password reset was requested for your Gotbee account. Please click the link below to change the password.</p>` +
          `<p><a href="${resetLink}">${resetLink}</a>&nbsp;</p>` + //<span style="border: 1px solid #ccc!important; border-radius: 16px; padding: 0.01em 16px;">${resetLink}</span>
          `<p>This link will expire 1 hour after it was requested.` +
          ` If you did not request a password reset, please contact your administrator and inform them of this notification.</p>` +
          `<p>-Gotbee</p>`;

        email.sendEmail(emailAddress, subject, text, html);
        callback(err, null);
      });
    });
  });
};

module.exports.forgotPasswordReset = function forgotPasswordReset(_id, email, password, callback) {
  console.log('TemporaryAuthentication.findById _id', _id);
  TemporaryAuthentication.findById(_id, (err, tempAuth) => {
    console.log('TemporaryAuthentication.findById err', err);
    console.log('TemporaryAuthentication.findById tempAuth', tempAuth);
    if (err) {
      return callback(err);
    } else if (!tempAuth || tempAuth.person.email.toLowerCase() !== email.toLowerCase()) {
      return callback(new Error('temporary authentication not found'));
    }

    const query = { _id: tempAuth.person._id };
    module.exports
      .updatePassword(query, password)
      .then(res => {
        return tempAuth.remove(() => {
          callback(null, res);
        });
      })
      .catch(reason => {
        callback(new Error(reason));
      });
  }).populate({ path: 'person', select: 'email _id' });
};

module.exports.updatePassword = function updatePassword(query, password) {
  return new Promise((accept, reject) => {
    const rules = {
      allowPassphrases: true,
      maxLength: 128,
      minLength: 10,
      minPhraseLength: 20
    };

    owasp.config(rules);

    const result = owasp.test(password);

    if (!result.strong) {
      return reject('weak password:' + result.errors.join(' '));
    }

    module.exports.HashPassword(password, (err, hashedPassword) => {
      const update = { private: { password: hashedPassword } };

      Person.findOneAndUpdate(query, update, function(err, person) {
        if (!person) {
          return reject('person not found');
        } else if (err) {
          return reject(err);
        }

        return accept({ result: 'success' });
      });
    });
  });
};

module.exports.HashPersonPassword = function(person, cb) {
  var password;
  //allow password to come in as person.password and person.private.password...
  if (person.private && person.private.password) {
    password = person.private.password;
  } else if (person.password) {
    password = person.password;
  }

  if (password) {
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
      if (err) return cb(err);

      // hash the password along with our new salt
      bcrypt.hash(password, salt, function(err, hash) {
        if (err) return cb(err);

        //move the password to the private sub-doc.
        if (!person.private) person.private = {};

        person.private.password = hash;
        if (person.password) delete person.password;

        return cb(null, person);
      });
    });
  } else {
    return cb(null, person);
  }
};

module.exports.HashPassword = (password, callback) => {
  //generate salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if (err) return callback(err);

    //generate hash using salt and password
    bcrypt.hash(password, salt, (err, hash) => {
      return callback(err, hash);
    });
  });
};
