const _ = require('lodash');
const express = require('express');
const router = express.Router();
const Project = require('../models/project');
const auth = require('../modules/auth');
const { allowed } = require('../modules/policy');
const { apiError } = require('../modules/utils');
const files = require('../modules/files');

const asyncify = fn => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(next);
};

const googleMapsClient = require('@google/maps').createClient({
  key: process.env.GOOGLE_MAPS_API_KEY,
  Promise
});

/*Helpers*/
function getQueryFromIdentifier(req) {
  if (auth.isSuperAdmin(req) === true) {
    return { _id: req.params.identifier };
  }
  return { _id: req.params.identifier, tenant: req.user.tenant };
}

const projectProjection = {
  tenant: false,
  __v: false
};

/* GET projects/
  gets multiple projects */
router.get(
  '/',
  asyncify(async (req, res) => {
    var query = {};
    if (auth.isSuperAdmin(req) !== true) {
      query = { tenant: req.user.tenant };
    }

    const projects = await Project.find(query, projectProjection);
    res.json(projects);
  })
);

/* POST /projects/
creates a project 
*/
router.post(
  '/',
  allowed,
  asyncify(async (req, res, next) => {
    const { address, longitude, latitude } = req.body;

    //assign same tenant as request and remove SuperAdmin permissions unless is superadmin
    if (auth.isSuperAdmin(req) !== true) {
      req.body.tenant = req.user.tenant;
    }

    if (address && !(longitude || latitude)) {
      const geo = await googleMapsClient.geocode({ address }).asPromise();
      if (_.get(geo, 'json.results[0].geometry.location.lat')) {
        const { lat, lng } = geo.json.results[0].geometry.location;
        req.body.latitude = lat;
        req.body.longitude = lng;
      }
    }

    const project = await Project.create(req.body);
    res.json(project);
  })
);

/* GET /projects/{identifier}
gets a projects */
router.get(
  '/:identifier',
  allowed,
  asyncify(async (req, res) => {
    const project = await Project.findOne(getQueryFromIdentifier(req), projectProjection);
    if (!project) throw Error(404);

    res.json(project);
  })
);

/* PUT /projects/{identifier}
  updates a project */
router.put(
  '/:identifier',
  allowed,
  asyncify(async (req, res, next) => {
    let prevProject = await Project.findOne(getQueryFromIdentifier(req), projectProjection);

    //standard fields

    //if address need updated
    if (
      req.body.address &&
      (req.body.address !== prevProject.address || !prevProject.latitude || !prevProject.longitude)
    ) {
      const geo = await googleMapsClient.geocode({ address: req.body.address }).asPromise();
      if (_.get(geo, 'json.results[0].geometry.location.lat')) {
        const { lat, lng } = geo.json.results[0].geometry.location;
        prevProject.latitude = lat;
        prevProject.longitude = lng;
      }
    }

    //set standard fields
    _.forOwn(req.body, (value, key) => {
      const standardFields = ['active', 'id', 'name', 'address'];
      if (standardFields.indexOf(key) > -1) prevProject[key] = value;
    });

    //custom fields
    const tenantCustomFields = req.user.tenant.projectsConfig.customFields;
    if (req.body.customFields && tenantCustomFields) {
      _.forOwn(req.body.customFields, (value, key) => {
        const tenantCustomField = _.find(tenantCustomFields, function(o) {
          return o._id == key;
        });
        if (tenantCustomField) {
          if (!prevProject.customFields) prevProject.customFields = {};

          switch (tenantCustomField.kind) {
            case 'date':
              prevProject.customFields.set(key, value ? new Date(value) : null);
              break;
            case 'number':
              prevProject.customFields.set(key, Number(value));
              break;
            case 'checkbox':
              prevProject.customFields.set(key, Boolean(value));
              break;
            default:
              prevProject.customFields.set(key, value);
              break;
          }
        }
      });
    }

    if (req.body.personAssignments) {
      prevProject.personAssignments = req.body.personAssignments;
    }

    const curProject = await prevProject.save();
    res.json(curProject);
  })
);

////Files////

/* GET /projects/{identifier}/files/put
gets a signed url to put a file in the storage. this does not alter the project record.
*/
router.get(
  '/:identifier/files/put',
  allowed,
  asyncify(async (req, res) => {
    const { name, type } = req.query;
    const _id = req.params.identifier;
    const tenant_id = req.user.tenant._id;
    const key = `tenants/${tenant_id}/projects/${_id}/files/${name}`;
    const url = await files.signedPutUrl(key, type);

    res.json({ url });
  })
);

/* GET /projects/{identifier}/files/{fileIdentifier}
  ammends the files record for a project by adding an entry
  */
router.get(
  '/:identifier/files/:fileId',
  allowed,
  asyncify(async (req, res) => {
    const fileId = req.params.fileId;
    let query = getQueryFromIdentifier(req);
    query['files._id'] = fileId;

    const project = await Project.findOne(query, { 'files.$': 1 });
    const file = project.files[0];

    const _id = req.params.identifier;
    const tenant_id = req.user.tenant._id;
    const key = `tenants/${tenant_id}/projects/${_id}/files/${file.name}`;

    const url = await files.signedGetUrl(key);

    res.json({ url });
  })
);

/* PUT /projects/{identifier}/files
  add/update the files record for a project by adding an entry for a file
  */
router.put(
  '/:identifier/files',
  allowed,
  asyncify(async (req, res) => {
    const { name, size } = req.body;

    if (!name) throw Error(400);

    let project = await Project.findOne(getQueryFromIdentifier(req), projectProjection);

    if (!project.files) project.files = [];

    const existingFile = project.files.find(function(element) {
      return element.name == name;
    });

    if (!existingFile) {
      project.files.push({ name, size });
    }

    const projectUpdated = await project.save();
    res.json(projectUpdated);
  })
);

/* DELETE /projects/{identifier}/files/{fileId}
  removed a file record for a project and removes file from storage
  */
router.delete(
  '/:identifier/files/:fileId',
  allowed,
  asyncify(async (req, res) => {
    const fileId = req.params.fileId;

    let query = getQueryFromIdentifier(req);
    query['files._id'] = fileId;

    const project = await Project.findOne(query, projectProjection);

    const file = project.files.find(function(element) {
      return element._id == fileId;
    });

    if (!file) throw Error(404);

    const _id = req.params.identifier;
    const tenant_id = req.user.tenant._id;
    const key = `tenants/${tenant_id}/projects/${_id}/files/${file.name}`;

    await files.deleteFile(key);
    project.files.remove({ _id: fileId });
    const curProject = await project.save();

    res.json(curProject);
  })
);

////Assignments////

/* GET  /people/{identifier}/assignments
get's a person's assignments */

/* POST  /people/{identifier}/assignments
creates a person's assignment */
router.post(
  '/:identifier/assignments',
  allowed,
  asyncify(async (req, res) => {
    let person = await Person.findOne(getQueryFromIdentifier(req), personProjection);
    if (!person.assignments) person.assignments = [];

    delete req.body._id;

    person.assignments.push(req.body);

    const personUpdated = await person.save();
    res.json(personUpdated);
  })
);

/* GET  /people/{identifier}/assignments/{identifier}
  gets a person's assignment */

/* PUT /people/{identifier}/assignments/{identifier}
  updates a person's assignment */
router.put(
  '/:identifier/assignments/:assignment_id',
  allowed,
  asyncify(async (req, res) => {
    const assignment_id = req.params.assignment_id;
    let person = await Person.findOne(getQueryFromIdentifier(req), personProjection);
    if (!person.assignments) person.assignments = [];

    let existingAssignment = person.assignments.find(function(element) {
      return element._id == assignment_id;
    });

    if (!existingAssignment) {
      throw Error(404);
    }

    delete req.body._id;
    existingAssignment = _.assign(existingAssignment, req.body);

    const personUpdated = await person.save();
    res.json(personUpdated);
  })
);

/* DELETE /people/{identifier}/assignments/{identifier}
  deletes a person's assignment */
router.delete(
  '/:identifier/assignments/:assignment_id',
  allowed,
  asyncify(async (req, res) => {
    const assignment_id = req.params.assignment_id;

    let query = getQueryFromIdentifier(req);
    query['assignments._id'] = assignment_id;

    const person = await Person.findOne(query, personProjection);

    const assignment = person.assignments.find(function(element) {
      return element._id == assignment_id;
    });

    if (!assignment) throw Error(404);

    person.assignments.remove({ _id: assignment_id });
    const curPerson = await person.save();

    res.json(curPerson);
  })
);

router.use(function(err, req, res, next) {
  //console.error(err);
  const { message } = err;

  if (err.message && err.message.startsWith('E11000 duplicate key')) {
    return res.status(409).json(apiError(409));
  } else if (message === '400' || message === '404') {
    return res.status(message).json(apiError(message));
  }

  return res.status(500).json(apiError(500));
});

module.exports = router;
