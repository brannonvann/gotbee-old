const express = require('express');
const router = express.Router();
const Person = require('../models/person');
const auth = require('../modules/auth');

/* GET people/
gets multiple people */
router.get('/all', (req, res) => {
  var query = {
    $or: [
      { name: { $regex: `.*${req.query.term}.*`, $options: 'i' } },
      { id: { $regex: `.*${req.query.term}.*`, $options: 'i' } }
    ]
  };
  if (auth.isSuperAdmin(req) !== true) {
    query.tenant = req.user.tenant;
  }

  const projection = {
    name: true,
    id: true,
    address: true,
    latitude: true,
    longitude: true,
    customValues: true
  };
  Person.find(query, projection, (err, people) => {
    if (err) {
      res.statusCode(500).send(error.message);
    }

    res.json({ people });
  });
});

module.exports = router;
