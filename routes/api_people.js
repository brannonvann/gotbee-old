const _ = require('lodash');
const express = require('express');
const router = express.Router();
const Person = require('../models/person');
const auth = require('../modules/auth');
const { allowed } = require('../modules/policy');
const { apiError } = require('../modules/utils');
const files = require('../modules/files');

//asyncMiddleware to allow function to be async/await
const asyncify = fn => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(next);
};

const googleMapsClient = require('@google/maps').createClient({
  key: process.env.GOOGLE_MAPS_API_KEY,
  Promise
});

/*Helpers*/
function getQueryFromIdentifier(req) {
  if (auth.isSuperAdmin(req) === true) {
    return { _id: req.params.identifier };
  }
  return { _id: req.params.identifier, tenant: req.user.tenant };
}

const personProjection = {
  private: false,
  tenant: false,
  __v: false
};

/* GET people/
gets multiple people */
router.get(
  '/',
  asyncify(async (req, res) => {
    var query = {};
    if (auth.isSuperAdmin(req) !== true) {
      query = { tenant: req.user.tenant };
    }

    const people = await Person.find(query, personProjection);
    res.json(people);
  })
);

/* POST /people/
creates a person 
*/
router.post(
  '/',
  allowed,
  asyncify(async (req, res, next) => {
    const { address, longitude, latitude } = req.body;

    //disallow setting of some properties.
    delete req.body.private;

    //assign same tenant as request and remove SuperAdmin permissions unless is superadmin
    if (auth.isSuperAdmin(req) !== true) {
      req.body.tenant = req.user.tenant;
      delete req.body.superAdmin;
    }

    if (address && !(longitude || latitude)) {
      const geo = await googleMapsClient.geocode({ address }).asPromise();
      if (_.get(geo, 'json.results[0].geometry.location.lat')) {
        const { lat, lng } = geo.json.results[0].geometry.location;
        req.body.latitude = lat;
        req.body.longitude = lng;
      }
    }

    const person = await Person.create(req.body);
    res.json(person);
  })
);

/* GET /people/{identifier}
gets a person */
router.get(
  '/:identifier',
  allowed,
  asyncify(async (req, res) => {
    const person = await Person.findOne(getQueryFromIdentifier(req), personProjection);
    if (!person) throw Error('person not found');

    res.json(person);
  })
);

/* PUT /people/{identifier}
updates a person */
router.put(
  '/:identifier',
  allowed,
  asyncify(async (req, res, next) => {
    delete req.body.private;

    //remove SuperAdmin permissions unless is superadmin
    if (!auth.isSuperAdmin(req)) delete req.body.superAdmin;

    let prevPerson = await Person.findOne(getQueryFromIdentifier(req), personProjection);

    //standard fields

    //if address need updated
    if (
      req.body.address &&
      (req.body.address !== prevPerson.address || !prevPerson.latitude || !prevPerson.longitude)
    ) {
      const geo = await googleMapsClient.geocode({ address: req.body.address }).asPromise();
      if (_.get(geo, 'json.results[0].geometry.location.lat')) {
        const { lat, lng } = geo.json.results[0].geometry.location;
        prevPerson.latitude = lat;
        prevPerson.longitude = lng;
      }
    }

    //set standard fields
    _.forOwn(req.body, (value, key) => {
      const standardFields = ['active', 'superAdmin', 'admin', 'id', 'name', 'email', 'address'];
      if (standardFields.indexOf(key) > -1) prevPerson[key] = value;
    });

    //custom fields
    const tenantCustomFields = req.user.tenant.peopleConfig.customFields;
    if (req.body.customFields && tenantCustomFields) {
      _.forOwn(req.body.customFields, (value, key) => {
        const tenantCustomField = _.find(tenantCustomFields, function(o) {
          return o._id == key;
        });
        if (tenantCustomField) {
          if (!prevPerson.customFields) prevPerson.customFields = {};

          switch (tenantCustomField.kind) {
            case 'date':
              prevPerson.customFields.set(key, value ? new Date(value) : null);
              break;
            case 'number':
              prevPerson.customFields.set(key, Number(value));
              break;
            case 'checkbox':
              prevPerson.customFields.set(key, Boolean(value));
              break;
            default:
              prevPerson.customFields.set(key, value);
              break;
          }
        }
      });
    }

    if (req.body.ratings) {
      prevPerson.ratings = req.body.ratings;
    }

    if (req.body.injuries) {
      prevPerson.injuries = req.body.injuries;
    }

    const curPerson = await prevPerson.save();
    res.json(curPerson);
  })
);

/* DELETE  /people/{identifier}
deletes a person */

/* PUT /people/{identifier}/password
updates a person's password */
router.put(
  '/:identifier/password',
  allowed,
  asyncify(async (req, res) => {
    const password = req.body.password.toString();
    const _id = req.params.identifier;

    //is admin or updating own password.
    if (!(auth.isAdmin(req) || req.user._id === _id)) {
      return res.status(403).json(apiError(403));
    }

    await auth.updatePassword(getQueryFromIdentifier(req), password);

    res.json({ result: 'success' });
  })
);

////Files////

/* GET /people/{identifier}/files/put
gets a signed url to put a file in the storage. this does not alter the person record.
*/
router.get(
  '/:identifier/files/put',
  allowed,
  asyncify(async (req, res) => {
    const { name, type } = req.query;
    const _id = req.params.identifier;
    const tenant_id = req.user.tenant._id;
    const key = `tenants/${tenant_id}/people/${_id}/files/${name}`;
    const url = await files.signedPutUrl(key, type);

    res.json({ url });
  })
);

/* GET /people/{identifier}/files/{fileIdentifier}
ammends the files record for a person by adding an entry
*/
router.get(
  '/:identifier/files/:fileId',
  allowed,
  asyncify(async (req, res) => {
    const fileId = req.params.fileId;
    let query = getQueryFromIdentifier(req);
    query['files._id'] = fileId;

    const person = await Person.findOne(query, { 'files.$': 1 });
    const file = person.files[0];

    const _id = req.params.identifier;
    const tenant_id = req.user.tenant._id;
    const key = `tenants/${tenant_id}/people/${_id}/files/${file.name}`;

    const url = await files.signedGetUrl(key);

    res.json({ url });
  })
);

/* PUT /people/{identifier}/files
add/update the files record for a person by adding an entry for a file
*/
router.put(
  '/:identifier/files',
  allowed,
  asyncify(async (req, res) => {
    const { name, size } = req.body;

    if (!name) throw Error(400);

    let person = await Person.findOne(getQueryFromIdentifier(req), personProjection);

    if (!person.files) person.files = [];

    const existingFile = person.files.find(function(element) {
      return element.name == name;
    });

    if (!existingFile) {
      person.files.push({ name, size });
    }

    const personUpdated = await person.save();
    res.json(personUpdated);
  })
);

/* DELETE /people/{identifier}/files/{fileId}
removed a file record for a person and removes file from storage
*/
router.delete(
  '/:identifier/files/:fileId',
  allowed,
  asyncify(async (req, res) => {
    const fileId = req.params.fileId;

    let query = getQueryFromIdentifier(req);
    query['files._id'] = fileId;

    const person = await Person.findOne(query, personProjection);

    const file = person.files.find(function(element) {
      return element._id == fileId;
    });

    if (!file) throw Error(404);

    const _id = req.params.identifier;
    const tenant_id = req.user.tenant._id;
    const key = `tenants/${tenant_id}/people/${_id}/files/${file.name}`;

    await files.deleteFile(key);
    person.files.remove({ _id: fileId });
    const curPerson = await person.save();

    res.json(curPerson);
  })
);

////Injuries////

/* GET  /people/{identifier}/injuries
get's a person's injuries */

/* POST  /people/{identifier}/injuries
creates a person's injury */
router.post(
  '/:identifier/injuries',
  allowed,
  asyncify(async (req, res) => {
    let person = await Person.findOne(getQueryFromIdentifier(req), personProjection);
    if (!person.injuries) person.injuries = [];

    delete req.body._id;

    person.injuries.push(req.body);

    const personUpdated = await person.save();
    res.json(personUpdated);
  })
);

/* GET  /people/{identifier}/injuries/{identifier}
gets a person's injury */

/* PUT /people/{identifier}/injuries/{identifier}
updates a person's injury */
//expects complete record.
router.put(
  '/:identifier/injuries/:injury_id',
  allowed,
  asyncify(async (req, res) => {
    const injury_id = req.params.injury_id;
    let person = await Person.findOne(getQueryFromIdentifier(req), personProjection);
    if (!person.injuries) person.injuries = [];

    let existingInjury = person.injuries.find(function(element) {
      return element._id == injury_id;
    });

    if (!existingInjury) {
      throw Error(404);
    }

    delete req.body._id;
    existingInjury = _.assign(existingInjury, req.body);

    const personUpdated = await person.save();
    res.json(personUpdated);
  })
);

/* DELETE /people/{identifier}/injuries/{identifier}
deletes a person's injury */
router.delete(
  '/:identifier/injuries/:injury_id',
  allowed,
  asyncify(async (req, res) => {
    const injury_id = req.params.injury_id;

    let query = getQueryFromIdentifier(req);
    query['injuries._id'] = injury_id;

    const person = await Person.findOne(query, personProjection);

    const injury = person.injuries.find(function(element) {
      return element._id == injury_id;
    });

    if (!injury) throw Error(404);

    person.injuries.remove({ _id: injury_id });
    const curPerson = await person.save();

    res.json(curPerson);
  })
);

////Ratings////

/* GET  /people/{identifier}/ratings
get's a person's ratings */

/* POST  /people/{identifier}/ratings
creates a person's rating */
router.post(
  '/:identifier/ratings',
  allowed,
  asyncify(async (req, res) => {
    let person = await Person.findOne(getQueryFromIdentifier(req), personProjection);
    if (!person.ratings) person.ratings = [];

    delete req.body._id;

    person.ratings.push(req.body);

    const personUpdated = await person.save();
    res.json(personUpdated);
  })
);

/* GET  /people/{identifier}/ratings/{identifier}
gets a person's rating */

/* PUT /people/{identifier}/ratings/{identifier}
updates a person's rating */
router.put(
  '/:identifier/ratings/:rating_id',
  allowed,
  asyncify(async (req, res) => {
    const rating_id = req.params.rating_id;
    let person = await Person.findOne(getQueryFromIdentifier(req), personProjection);
    if (!person.ratings) person.ratings = [];

    let existingRating = person.ratings.find(function(element) {
      return element._id == rating_id;
    });

    if (!existingRating) {
      throw Error(404);
    }

    delete req.body._id;
    existingRating = _.assign(existingRating, req.body);

    const personUpdated = await person.save();
    res.json(personUpdated);
  })
);

/* DELETE /people/{identifier}/ratings/{identifier}
deletes a person's rating */
router.delete(
  '/:identifier/ratings/:rating_id',
  allowed,
  asyncify(async (req, res) => {
    const rating_id = req.params.rating_id;

    let query = getQueryFromIdentifier(req);
    query['ratings._id'] = rating_id;

    const person = await Person.findOne(query, personProjection);

    const rating = person.ratings.find(function(element) {
      return element._id == rating_id;
    });

    if (!rating) throw Error(404);

    person.ratings.remove({ _id: rating_id });
    const curPerson = await person.save();

    res.json(curPerson);
  })
);

router.use(function(err, req, res, next) {
  console.error(err);
  const { message } = err;

  if (err.message && err.message.startsWith('E11000 duplicate key')) {
    return res.status(409).json(apiError(409));
  } else if (typeof stringValue === 'string' && err.startsWith('weak password:')) {
    //password update response
    const details = err.substring(14);
    return res.status(400).json(apiError(400, details));
  } else if (err === 'person not found') {
    return res.status(404).json(apiError(404));
  } else if (message === '400' || message === '404') {
    return res.status(message).json(apiError(message));
  }

  return res.status(500).json(apiError(500));
});

module.exports = router;
