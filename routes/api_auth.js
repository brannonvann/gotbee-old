const express = require('express');
const router = express.Router();
const auth = require('../modules/auth');
const passport = require('passport');
const Recaptcha = require('express-recaptcha').Recaptcha;
const { apiError } = require('../modules/utils');
const recaptcha = new Recaptcha(process.env.RECAPTCHA_SITE_KEY, process.env.RECAPTCHA_SECRET_KEY);

router.get('/profile', (req, res) => {
  res.send(req.user);
});

//pwrr : password reset request (forgot password)
router.post('/forgotpwrequest', recaptcha.middleware.verify, (req, res) => {
  if (req.recaptcha.error) {
    res.status(400).json(apiError(400, 'reCAPTCHA incomplete.'));
    return;
  }
  //recaptcha was success
  const email = req.body.email;

  auth.forgotPasswordRequest(email, err => {
    if (err) console.log('forgotPasswordRequest error', err);
    //return 200 and "succes" even if
    //there was an error to prevent id lookups
    res.json({ status: 'success' });
  });
});

//pwr: password reset (reasponse to forgot password)
router.post('/forgotpwreset/:identifier', (req, res) => {
  const _id = req.params.identifier;
  const { password, email } = req.body;
  auth.forgotPasswordReset(_id, email, password, err => {
    if (err) {
      if (err.message.startsWith('weak password:')) {
        const details = err.message.substring(14);
        return res.status(400).json(apiError(400, details));
      } else if (
        err.message === 'person not found' ||
        err.message === 'temporary authentication not found'
      ) {
        return res.status(404).json(apiError(404));
      } else {
        return res.status(500).json(apiError(500));
      }
    }
    res.json({ result: 'success' });
  });
});

router.post('/logout', (req, res) => {
  req.logout();
  res.end();
});

router.post('/login', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) {
      return next(err);
    }

    if (!user) {
      return res.status(401).json(apiError(401, info.error));
    }

    req.logIn(user, function(err) {
      if (err) {
        return next(err);
      }
      res.json({ status: 'success' });
    });
  })(req, res, next);
});

module.exports = router;
