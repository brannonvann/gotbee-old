const express = require('express');
const router = express.Router();
const Tenant = require('../models/tenant');
const mongoose = require('mongoose');
const { allowed } = require('../modules/policy');
const auth = require('../modules/auth');
var HttpStatus = require('http-status-codes');
const { apiError } = require('../modules/utils');

//asyncMiddleware to allow function to be async/await
const asyncify = fn => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(next);
};

/* GET /tenants/ 
Gets multiple tenants*/
router.get('/', (req, res) => {
  if (auth.isSuperAdmin(req) !== true) {
    res.status(HttpStatus.FORBIDDEN);
    return;
  }

  Tenant.find((err, tenants) => {
    res.json(tenants);
  });
});

/* POST /tenants/ 
Creates a tenant */
router.post('/', (req, res) => {
  if (auth.isSuperAdmin(req) !== true) {
    return res.status(403).json(apiError(403));
  }

  var options = { upsert: true, returnNewDocument: true };

  Tenant.create(req.body, options, (err, tenant) => {
    res.json(tenant);
  });
});

/*GET /tenants/{identifier} 
Gets a tenant*/
router.get('/:identifier', (req, res) => {
  if (auth.isSuperAdmin(req) !== true) {
    res.status(HttpStatus.FORBIDDEN);
    return;
  }

  const query = { _id: req.params.identifier };

  Tenant.findOne(query, (err, tenant) => {
    res.json(tenant);
  });
});

/*PUT /tenants/{identifier}
Updates a tenant*/
router.put('/:identifier', (req, res) => {
  if (auth.isSuperAdmin(req) !== true) {
    res.status(HttpStatus.FORBIDDEN);
    return;
  }

  const query = { _id: req.params.identifier };
  const options = { returnNewDocument: true };

  Tenant.findOneAndUpdate(query, req.body, options, (err, tenant) => {
    res.json(tenant);
  });
});

/*DELETE tenant /api/tenants/{identifier} 
Deletes a tenant*/
router.delete('/:identifier', (req, res) => {
  if (auth.isSuperAdmin(req) !== true) {
    res.status(HttpStatus.FORBIDDEN);
    return;
  }

  const query = { _id: req.params.identifier };

  Tenant.findOneAndDelete(query, tenant => {
    res.json(tenant);
  });
});

////Peopl Views////

/* POST  /tenants/{identifier}/peopleconfig/views
creates a tenant's view */
router.post(
  '/:identifier/peopleconfig/views',
  allowed,
  asyncify(async (req, res) => {
    const query = { _id: req.params.identifier };
    const tenant = await Tenant.findOne(query);
    if (!tenant.peopleConfig) tenant.peopleConfig = {};
    if (!tenant.peopleConfig.views) tenant.peopleConfig.views = [];

    delete req.body._id;

    tenant.peopleConfig.views.push(req.body);

    const tenantUpdated = await tenant.save();
    res.json({ success: true });
  })
);

/* PUT /people/{identifier}/peopleconfig/views/{identifier}
  updates a tenant's rating */
router.put(
  '/:identifier/peopleconfig/views/:view_id',
  allowed,
  asyncify(async (req, res) => {
    const view_id = req.params.view_id;
    let tenant = await Tenant.findOne(getQueryFromIdentifier(req), tenantProjection);
    if (!tenant.peopleConfig) tenant.peopleConfig = {};
    if (!tenant.peopleConfig.views) tenant.peopleConfig.views = [];

    let existingView = tenant.peopleConfig.views.find(function(element) {
      return element._id == view_id;
    });

    if (!existingView) {
      throw Error(404);
    }

    delete req.body._id;
    existingView = _.assign(existingView, req.body);

    const tenantUpdated = await tenant.save();
    res.json({ success: true });
  })
);

/* POST  /tenants/{identifier}/peopleconfig/views
creates a tenant's view */
router.post(
  '/:identifier/projectconfig/customfields',
  allowed,
  asyncify(async (req, res) => {
    const query = { _id: req.params.identifier };
    const tenant = await Tenant.findOne(query);
    delete req.body._id;

    tenant.projectConfig.customFields.push(req.body);

    const tenantUpdated = await tenant.save();
    res.json({ success: true });
  })
);

module.exports = router;
