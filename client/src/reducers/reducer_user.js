import { STORE_PEOPLE_SELECTED_COLUMNS } from '../actions/types';

const initialState = {
  people: { selectedColumns: 'name,id' },
  projects: { selectedColumns: 'name,id' }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case STORE_PEOPLE_SELECTED_COLUMNS:
      return Object.assign({}, state, { people: { selectedColumns: action.selectedColumns } });
    default:
      return state;
  }
}
