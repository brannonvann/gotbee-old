import { UPDATE_TABLE_CONFIGURATION } from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case UPDATE_TABLE_CONFIGURATION: {
      const { tableName, configuration } = action;
      const table = Object.assign({}, state[tableName], configuration);
      return Object.assign({}, state, { [tableName]: table });
    }
    default:
      return state;
  }
}
