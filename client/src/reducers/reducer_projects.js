import _ from 'lodash';
import {
  FETCH_PROJECTS,
  FETCH_PROJECT,
  DELETE_PROJECT,
  UPDATE_PROJECT,
  CREATE_PROJECT,
  UPLOAD_PROJECT_FILE,
  DELETE_PROJECT_FILE
} from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_PROJECTS:
      return _.mapKeys(action.payload, '_id');
    case FETCH_PROJECT:
      return { ...state, [action.payload._id]: action.payload };
    case UPDATE_PROJECT:
      return { ...state, [action.payload._id]: action.payload };
    case DELETE_PROJECT:
      return _.omit(state, action.payload);
    case CREATE_PROJECT:
      return state;
    case UPLOAD_PROJECT_FILE:
      return { ...state, [action.payload._id]: action.payload };
    case DELETE_PROJECT_FILE:
      return { ...state, [action.payload._id]: action.payload };
    default:
      return state;
  }
}
