import _ from 'lodash';
import { DELETE_ALERT, CREATE_ALERT, DELETE_ALERTS_ALL } from '../actions/types';

export default function(state = [], action) {
  switch (action.type) {
    case CREATE_ALERT: {
      return _.concat(state, action.alert);
    }

    case DELETE_ALERT: {
      return _.without(state, action.alert);
    }

    case DELETE_ALERTS_ALL: {
      return [];
    }

    default:
      return state;
  }
}
