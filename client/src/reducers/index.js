import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import AuthReducer from './reducer_auth';
import PeopleReducer from './reducer_people';
import ProjectsReducer from './reducer_projects';
import SearchReducer from './reducer_search';
import AlertsReducer from './reducer_alerts';
import UserReducer from './reducer_user';
import TablesReducer from './reducer_tables';

const rootReducer = combineReducers({
  auth: AuthReducer,
  form: formReducer, //the name "form" is important for this to work
  people: PeopleReducer,
  projects: ProjectsReducer,
  search: SearchReducer,
  alerts: AlertsReducer,
  user: UserReducer,
  tables: TablesReducer
});

export default rootReducer;
