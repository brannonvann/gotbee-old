import { FETCH_PROFILE } from '../actions/types';
//import moment from 'moment';

const initalState = {
  profile: null
};

export default function(state = initalState, action) {
  switch (action.type) {
    case FETCH_PROFILE: {
      return {
        profile: action.payload || false //loggedIn: action.payload.data ? true : false
      };
    }
    default:
      return state;
  }
}
