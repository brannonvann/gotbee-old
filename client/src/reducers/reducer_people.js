import _ from 'lodash';
import {
  FETCH_PEOPLE,
  FETCH_PERSON,
  DELETE_PERSON,
  UPDATE_PERSON,
  CREATE_PERSON,
  UPLOAD_PERSON_FILE,
  DELETE_PERSON_FILE
} from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_PEOPLE:
      return _.mapKeys(action.payload, '_id');
    case FETCH_PERSON:
      return { ...state, [action.payload._id]: action.payload };
    case UPDATE_PERSON:
      return { ...state, [action.payload._id]: action.payload };
    case DELETE_PERSON:
      return _.omit(state, action.payload);
    case CREATE_PERSON:
      return state;
    case UPLOAD_PERSON_FILE:
      return { ...state, [action.payload._id]: action.payload };
    case DELETE_PERSON_FILE:
      return { ...state, [action.payload._id]: action.payload };
    default:
      return state;
  }
}
