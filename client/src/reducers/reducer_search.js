import _ from 'lodash';
import {
  FETCH_ALL_SEARCH,
  SET_SEARCH_TERM,
  SEARCH_ALL_CLEAR
} from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_ALL_SEARCH:
      return {
        ...state,
        people: _.mapKeys(action.payload.data.people, 'id')
      };
    case SET_SEARCH_TERM:
      return {
        ...state,
        term: action.term
      };
    case SEARCH_ALL_CLEAR:
      return { term: '' };
    default:
      return state;
  }
}
