export const config = {
  standardFields: {
    active: {
      name: 'Active',
      _id: 'active',
      kind: 'checkbox'
    },
    admin: {
      name: 'Admin',
      _id: 'admin',
      kind: 'checkbox'
    },
    id: {
      name: 'ID',
      _id: 'id',
      kind: 'line',
      required: true
    },
    name: {
      name: 'Name',
      _id: 'name',
      kind: 'line',
      required: true
    },
    email: {
      name: 'Email',
      _id: 'email',
      kind: 'email'
    },
    address: {
      name: 'Address',
      _id: 'address',
      kind: 'line'
    }
  },
  rating: {
    standardFields: {
      occurred: {
        name: 'Occurred',
        _id: 'occurred',
        kind: 'date',
        required: true
      },
      overallRating: {
        name: 'Rating',
        _id: 'overallRating',
        kind: 'line',
        required: true
      },
      notes: {
        name: 'Notes',
        _id: 'notes',
        kind: 'line'
      }
    },
    view: {
      table: { columns: ['occurred', 'overallRating', 'notes'] },
      groups: [
        {
          _id: 'rating_group_1',
          columns: [
            {
              _id: 'rating_col_1',
              items: [
                {
                  _id: 'occurred',
                  standardField: 'occurred'
                },
                {
                  _id: 'overallRating',
                  standardField: 'overallRating'
                }
              ]
            },
            {
              _id: 'rating_col_2',
              items: [
                {
                  _id: 'notes',
                  standardField: 'notes'
                }
              ]
            }
          ]
        }
      ]
    }
  },
  injury: {
    standardFields: {
      occurred: {
        name: 'Occurred',
        _id: 'occurred',
        kind: 'date',
        required: true
      },
      overseeingPerson: {
        name: 'Responsible Person',
        _id: 'overseeingPerson',
        kind: 'record',
        record: 'people',
        required: true
      },
      notes: {
        name: 'Notes',
        _id: 'notes',
        kind: 'multiline'
      },
      classification: {
        name: 'Classification',
        _id: 'classification',
        kind: 'line'
      },
      project: {
        name: 'Project',
        _id: 'project',
        kind: 'record',
        record: 'projects'
      }
    },
    view: {
      table: { columns: ['occurred', 'classification', 'notes'] },
      groups: [
        {
          _id: 'rating_group_1',
          columns: [
            {
              _id: 'rating_col_1',
              items: [
                {
                  _id: 'occurred',
                  standardField: 'occurred'
                },
                {
                  _id: 'overseeingPerson',
                  standardField: 'overseeingPerson'
                },
                {
                  _id: 'classification',
                  standardField: 'classification'
                },
                {
                  _id: 'project',
                  standardField: 'project'
                }
              ]
            },
            {
              _id: 'rating_col_2',
              items: [
                {
                  _id: 'notes',
                  standardField: 'notes'
                }
              ]
            }
          ]
        }
      ]
    }
  }
};
