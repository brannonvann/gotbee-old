export const config = {
  standardFields: {
    active: {
      name: 'Active',
      _id: 'active',
      kind: 'checkbox'
    },
    id: {
      name: 'ID',
      _id: 'id',
      kind: 'line',
      required: true
    },
    name: {
      name: 'Name',
      _id: 'name',
      kind: 'line',
      required: true
    },
    address: {
      name: 'Address',
      _id: 'address',
      kind: 'line'
    }
  },
  personAssignments: {
    standardFields: {
      person: {
        name: 'Person',
        _id: 'person',
        kind: 'record',
        record: 'people',
        required: true
      },
      start: {
        name: 'Start',
        _id: 'start',
        kind: 'date'
      },
      end: {
        name: 'End',
        _id: 'end',
        kind: 'date'
      }
    },
    view: {
      table: { columns: ['person', 'start', 'end'] },
      groups: [
        {
          _id: 'person_assignment_group_1',
          columns: [
            {
              _id: 'person_assignment_col_1',
              items: [
                {
                  _id: 'person',
                  standardField: 'person'
                }
              ]
            },
            {
              _id: 'person_assignment_col_2',
              items: [
                {
                  _id: 'start',
                  standardField: 'start'
                },
                {
                  _id: 'end',
                  standardField: 'end'
                }
              ]
            }
          ]
        }
      ]
    }
  }
};
