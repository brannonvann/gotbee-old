import React, { Component } from 'react';
import { connect } from 'react-redux';

import ForgotPasswordResetForm from '../components/ForgotPasswordResetForm';
import FormContainerSmall from '../components/FormContainerSmall';

class PasswordReset extends Component {
  render() {
    const { id } = this.props.match.params;
    return (
      <FormContainerSmall>
        <ForgotPasswordResetForm id={id} />
      </FormContainerSmall>
    );
  }
}

export default connect()(PasswordReset);
