import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { logoutLocal } from '../actions';

import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

//import UniversalSearch from '../components/UniversalSearch';

class Header extends Component {
  constructor(props) {
    super(props);

    this.handleLogout = this.handleLogout.bind(this);
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  handleLogout() {
    this.props.logoutLocal().then(() => {
      window.location = '/login'; //redirect with refresh, if successful.
    });
  }

  renderContent() {
    const { profile } = this.props;

    switch (profile) {
      case null:
        return;
      case false:
        return (
          <Nav className="ml-auto justify-content-center" navbar>
            <NavItem>
              <Link className="nav-link text-dark" to="/login">
                Log In
              </Link>
            </NavItem>
          </Nav>
        );
      default:
        return [
          <Nav key="0" navbar>
            <NavItem>
              <Link className="nav-link text-secondary" to={`/people`}>
                People
              </Link>
            </NavItem>
            <NavItem>
              <Link className="nav-link text-secondary" to={`/projects`}>
                Projects
              </Link>
            </NavItem>
          </Nav>,
          <Nav key="1" className="ml-auto" navbar>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                <span className="oi oi-person pr-2 " />
                {profile.name}
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  <Link className="text-dark" to={`/people/${this.props.profile._id}/password`}>
                    Change Password
                  </Link>
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem onClick={this.handleLogout}>Log Out</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        ];
    }
  }

  /*
          
          */

  render() {
    const { profile } = this.props;
    const { isOpen } = this.state;

    return (
      <div className="navbar-wrapper">
        <Navbar light color="white" fixed="top" expand="md">
          <Link to={profile ? '/people' : '/login'} className="navbar-brand text-dark">
            gotbee
          </Link>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={isOpen} navbar>
            {this.renderContent()}
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

function mapStateToProps({ auth }) {
  return { profile: auth.profile };
}

export default connect(
  mapStateToProps,
  { logoutLocal }
)(Header);
