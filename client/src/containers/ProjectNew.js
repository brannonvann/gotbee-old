import React, { Component } from 'react';
import { connect } from 'react-redux';

import { createProject } from '../actions';

import NewMainRecord from '../components/NewPrimaryRecord';

class ProjectNew extends Component {
  render() {
    const { createProject } = this.props;
    return (
      <NewMainRecord
        title="Create a new project"
        createWith={createProject}
        redirectToAfterCreate="/projects"
      />
    );
  }
}

export default connect(
  null,
  { createProject }
)(ProjectNew);
