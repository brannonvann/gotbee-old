import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchPeople } from '../actions';
import { config } from '../config/people';

import PrimaryRecords from '../components/PrimaryRecords';

class People extends Component {
  render() {
    const { fetchPeople, profile, people } = this.props;
    const { customFields } = profile.tenant.peopleConfig;
    return (
      <div>
        <h3 className="d-inline-block align-middle mr-2">People</h3>
        <div className="d-inline">
          <Link className="btn btn-sm btn-outline-success" to={`/people/new`}>
            <span className="oi oi-plus" /> Add
          </Link>
        </div>
        <PrimaryRecords
          path="/people"
          tableName="people"
          defaultColumns="name,id"
          fetchRecords={fetchPeople}
          config={config}
          customFields={customFields}
          records={people}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    people: _.values(state.people),
    profile: state.auth.profile
  };
}

export default connect(
  mapStateToProps,
  {
    fetchPeople
  }
)(People);
