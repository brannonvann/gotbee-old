import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import LoginLocalForm from '../components/LoginLocalForm';
import FormContainerSmall from '../components/FormContainerSmall';

class Login extends Component {
  render() {
    return (
      <FormContainerSmall>
        <LoginLocalForm>
          <Link className="btn btn-link" to="/forgotpassword">
            Forgot Password?
          </Link>
        </LoginLocalForm>
      </FormContainerSmall>
    );
  }
}

export default connect()(Login);
