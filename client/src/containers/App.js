import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../actions';

import Header from './Header';
import Alerts from '../components/Alerts';
import Login from './Login';
import Forgot_Password_Reset from './Forgot_Password_Reset';
import Forgot_Password_Request from './Forgot_Password_Request';
import Projects from './Projects';
import Project from './Project';
import ProjectNew from './ProjectNew';
import People from './People';
import Person from './Person';
import PersonNew from './PersonNew';
import Person_Update_Password from './Person_Update_Password';
import PrivateRoute from '../components/PrivateRoute';
import EditViews from './EditViews';

class App extends Component {
  componentDidMount() {
    this.props.fetchProfile();
  }

  render() {
    return (
      <div>
        <BrowserRouter>
          <div>
            <div className="container-fluid">
              <Header />
              <Alerts />
            </div>
            <div className="container-fluid content-container">
              <Switch>
                <PrivateRoute path="/admin/people/views" component={EditViews} />
                <PrivateRoute path="/projects/new" component={ProjectNew} />
                <PrivateRoute path="/projects/:id" component={Project} />
                <PrivateRoute path="/projects" component={Projects} />
                <PrivateRoute path="/people/new" component={PersonNew} />
                <PrivateRoute path="/people/:id/password" component={Person_Update_Password} />
                <PrivateRoute path="/people/:id" component={Person} />
                <PrivateRoute path="/people" component={People} />
                <Route path="/login" component={Login} />
                <Route path="/forgotpassword" component={Forgot_Password_Request} />
                <Route path="/fpwr/:id" component={Forgot_Password_Reset} />
                <Route path="/*" component={createRedirect('/people')} />
                {/*  <Route component={NoMatch} /> */}
              </Switch>
            </div>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

const createRedirect = to => () => <Redirect to={to} />;

/* const NoMatch = ({ location }) => (
  <div>
    <h3>
      No match for <code>{location.pathname}</code>
    </h3>
  </div>
); */

export default connect(
  null,
  actions
)(App);
