/* 
2018.08.14 - Incomplete implementation of drag/drop view editor. 
this is intended to be a test and implemented with actual group, coulmn,
and items as rendered when view is used to display data.

Reference: https://medium.freecodecamp.org/reactjs-implement-drag-and-drop-feature-without-using-external-libraries-ad8994429f1a 
*/

import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
//import { Link } from 'react-router-dom';
//import { } from '../actions';

class EditViews extends Component {
  constructor(props) {
    super(props);
    this.state = { customFields: {} };

    this.renderItems = this.renderItems.bind(this);
  }

  componentWillMount() {
    const customFields = _.mapKeys(this.props.profile.tenant.peopleConfig.customFields, '_id');
    this.setState({ customFields });
  }

  renderCustomFields(customFields) {
    return _.map(customFields, customField => {
      return (
        <li className="list-group-item" key={customField._id}>
          {customField.name}
        </li>
      );
    });
  }

  renderItems(items) {
    const { customFields } = this.state;
    if (!customFields) return null;

    console.log('customFields', customFields);

    return _.map(items, item => {
      const label = customFields[item.customField]
        ? customFields[item.customField].name
        : item.standardField || item.tool;
      return (
        <li className="list-group-item" key={item._id}>
          {label}
        </li>
      );
    });
  }

  renderColumns(columns) {
    return _.map(columns, column => {
      return (
        <div className="list-group column" key={column._id}>
          {this.renderItems(column.items)}
        </div>
      );
    });
  }

  renderGroups(groups) {
    return _.map(groups, group => {
      return (
        <div className="group" key={group._id}>
          {group.name}
          {this.renderColumns(group.columns)}
        </div>
      );
    });
  }

  render() {
    const { customFields, views } = this.props.profile.tenant.peopleConfig;
    const { groups } = views[0];
    return (
      <div>
        <div>
          <div className="groups">{this.renderGroups(groups)}</div>
        </div>
        <ul className="list-group">{this.renderCustomFields(customFields)}</ul>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    profile: state.auth.profile
  };
}

export default connect(
  mapStateToProps,
  {}
)(EditViews);
