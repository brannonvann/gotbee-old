import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchProjects } from '../actions';
import { config } from '../config/projects';

import PrimaryRecords from '../components/PrimaryRecords';

class Projects extends Component {
  render() {
    const { fetchProjects, profile, projects } = this.props;
    const { projectsConfig } = profile.tenant;
    const customFields =
      projectsConfig && projectsConfig.customFields ? projectsConfig.customFields : [];

    return (
      <div>
        <h3 className="d-inline-block align-middle mr-2">Projects</h3>
        <div className="d-inline">
          <Link className="btn btn-sm btn-outline-success" to={`/projects/new`}>
            <span className="oi oi-plus" /> Add
          </Link>
        </div>
        <PrimaryRecords
          path="/projects"
          tableName="projects"
          defaultColumns="name,id"
          fetchRecords={fetchProjects}
          saveConfig={this.doNothing}
          config={config}
          customFields={customFields}
          records={projects}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    projects: _.values(state.projects),
    profile: state.auth.profile
  };
}

export default connect(
  mapStateToProps,
  {
    fetchProjects
  }
)(Projects);
