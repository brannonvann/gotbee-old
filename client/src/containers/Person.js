import React, { Component } from 'react';

import { connect } from 'react-redux';
import {
  fetchPerson,
  updatePerson,
  uploadPersonFile,
  getPersonFile,
  deletePersonFile
} from '../actions';

import { config } from '../config/people';

import PrimaryRecord from '../components/PrimaryRecord';

class PersonEditForm extends Component {
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.fetchPerson(id);

    this.uploadFileHandler = this.uploadFileHandler.bind(this);
    this.openFileHandler = this.openFileHandler.bind(this);
    this.deleteFileHandler = this.deleteFileHandler.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
  }

  uploadFileHandler(file) {
    const { id } = this.props.match.params;
    this.props.uploadPersonFile(id, file);
  }

  openFileHandler(fileId) {
    const { id } = this.props.match.params;
    this.props.getPersonFile(id, fileId);
  }

  deleteFileHandler(fileId) {
    const { id } = this.props.match.params;
    this.props.deletePersonFile(id, fileId);
  }

  onUpdate(values) {
    this.props.updatePerson(this.props.person._id, values);
  }

  render() {
    const { person, profile, match } = this.props;
    const { peopleConfig } = profile.tenant;
    const view = peopleConfig && peopleConfig.views[0] ? peopleConfig.views[0] : null;
    const customFields = peopleConfig && peopleConfig.customFields ? peopleConfig.customFields : [];

    if (!view) {
      return (
        <div>
          <p>You do not have a view available for this record.</p>
        </div>
      );
    }

    return (
      <PrimaryRecord
        match={match}
        view={view}
        config={config}
        uploadFileHandler={this.uploadFileHandler}
        openFileHandler={this.openFileHandler}
        deleteFileHandler={this.deleteFileHandler}
        onUpdate={this.onUpdate}
        record={person}
        initialValues={person}
        customFields={customFields}
        validate={validate}
        formName="PersonEditForm"
      />
    );
  }
}

function mapStateToProps({ auth, people }, ownProps) {
  return {
    person: people[ownProps.match.params.id],
    profile: auth.profile
  };
}

function validate(values) {
  const errors = {};
  if (!values.id) {
    errors.id = 'Enter an id!';
  }

  if (!values.name) {
    errors.name = 'Enter a name!';
  }
  return errors;
}

// You have to connect() to any reducers that you wish to connect to yourself
PersonEditForm = connect(
  mapStateToProps,
  {
    fetchPerson,
    updatePerson,
    uploadPersonFile,
    getPersonFile,
    deletePersonFile
  } // bind account loading action creator
)(PersonEditForm);

export default PersonEditForm;
