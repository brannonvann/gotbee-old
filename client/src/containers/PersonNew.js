import React, { Component } from 'react';
import { connect } from 'react-redux';

import { createPerson } from '../actions';

import NewMainRecord from '../components/NewPrimaryRecord';

class PersonNew extends Component {
  render() {
    const { createPerson } = this.props;
    return (
      <NewMainRecord
        title="Create a new person"
        createWith={createPerson}
        redirectToAfterCreate="/people"
      />
    );
  }
}

export default connect(
  null,
  { createPerson }
)(PersonNew);
