import React, { Component } from 'react';

import { connect } from 'react-redux';
import {
  fetchProject,
  updateProject,
  uploadProjectFile,
  getProjectFile,
  deleteProjectFile
} from '../actions';

import { config } from '../config/projects';

import PrimaryRecord from '../components/PrimaryRecord';

class ProjectEditForm extends Component {
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.fetchProject(id);

    this.uploadFileHandler = this.uploadFileHandler.bind(this);
    this.openFileHandler = this.openFileHandler.bind(this);
    this.deleteFileHandler = this.deleteFileHandler.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
  }

  uploadFileHandler(file) {
    const { id } = this.props.match.params;
    this.props.uploadProjectFile(id, file);
  }

  openFileHandler(fileId) {
    const { id } = this.props.match.params;
    this.props.getProjectFile(id, fileId);
  }

  deleteFileHandler(fileId) {
    const { id } = this.props.match.params;
    this.props.deleteProjectFile(id, fileId);
  }

  onUpdate(values) {
    this.props.updateProject(this.props.project._id, values);
  }

  render() {
    const { project, profile, match } = this.props;
    const { projectsConfig } = profile.tenant;
    const view = projectsConfig && projectsConfig.views[0] ? projectsConfig.views[0] : null;
    const customFields =
      projectsConfig && projectsConfig.customFields ? projectsConfig.customFields : [];

    if (!view) {
      return (
        <div>
          <p>You do not have a view available for this record.</p>
        </div>
      );
    }

    return (
      <PrimaryRecord
        match={match}
        view={view}
        config={config}
        uploadFileHandler={this.uploadFileHandler}
        openFileHandler={this.openFileHandler}
        deleteFileHandler={this.deleteFileHandler}
        onUpdate={this.onUpdate}
        record={project}
        initialValues={project}
        customFields={customFields}
        validate={validate}
        formName="ProjectEditForm"
      />
    );
  }
}

function mapStateToProps({ auth, projects }, ownProps) {
  return {
    project: projects[ownProps.match.params.id],
    profile: auth.profile
  };
}

function validate(values) {
  const errors = {};
  if (!values.id) {
    errors.id = 'Enter an id!';
  }

  if (!values.name) {
    errors.name = 'Enter a name!';
  }
  return errors;
}

// Decorate with reduxForm(). It will read the initialValues prop provided by connect()

// You have to connect() to any reducers that you wish to connect to yourself
ProjectEditForm = connect(
  mapStateToProps,
  {
    fetchProject,
    updateProject,
    uploadProjectFile,
    getProjectFile,
    deleteProjectFile
  } // bind account loading action creator
)(ProjectEditForm);

export default ProjectEditForm;
