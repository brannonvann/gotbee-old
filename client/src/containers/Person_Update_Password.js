import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import { updatePersonPassword } from '../actions';

import InputField from '../components/inputs/InputField';
import FormContainerSmall from '../components/FormContainerSmall';
import PasswordRules from '../components/PasswordRules';

class UpdatePersonPassword extends Component {
  renderField(field) {
    return <InputField field={field} />;
  }

  onSubmit(values) {
    //console.log('values', values);
    const { id } = this.props.match.params;

    this.props.updatePersonPassword(id, values, () => {
      this.props.history.push(`/people/${id}`);
    });
  }

  render() {
    const { handleSubmit, pristine, submitting, invalid } = this.props;
    return (
      <FormContainerSmall>
        <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
          <h5>Please enter the new password below</h5>
          <Field label="Password" kind="password" name="password" component={this.renderField} />
          <Field
            label="Confirm Password"
            kind="password"
            name="confirm-password"
            component={this.renderField}
          />
          <button
            className="btn btn-primary"
            type="submit"
            disabled={pristine || submitting || invalid}
          >
            Update Password
          </button>
        </form>
        <PasswordRules />
      </FormContainerSmall>
    );
  }
}

function validate(values) {
  const errors = {};

  if (!values.password) {
    errors.password = 'Enter a password!';
  }

  if (values.password !== values['confirm-password']) {
    errors['confirm-password'] = 'Passwords do not match!';
  }
  return errors;
}

export default reduxForm({
  validate,
  form: 'UpdatePersonPasswordForm' //think of as the name of the form. needs to be unique!
})(
  connect(
    null,
    { updatePersonPassword }
  )(UpdatePersonPassword)
);
