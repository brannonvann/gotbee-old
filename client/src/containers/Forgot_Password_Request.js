import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import ForgotPasswordRequestForm from '../components/ForgotPasswordRequestForm';
import FormContainerSmall from '../components/FormContainerSmall';

class ForgotPasswordRequest extends Component {
  render() {
    return (
      <FormContainerSmall>
        <ForgotPasswordRequestForm>
          <Link className="btn btn-link" to="/login">
            Remember password?
          </Link>
        </ForgotPasswordRequestForm>
      </FormContainerSmall>
    );
  }
}

export default connect()(ForgotPasswordRequest);
