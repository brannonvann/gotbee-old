//user
export const FETCH_PROFILE = 'fetch-profile';
export const LOGIN_LOCAL = 'login-local';
export const LOGOUT_LOCAL = 'logout-local';
export const FORGOT_PASSWORD_REQUEST = 'forgot-password-request';
export const FORGOT_PASSWORD_RESET = 'forgot-password-reset';
export const UPDATE_TABLE_CONFIGURATION = 'update-table-configuration';

//people
export const FETCH_PEOPLE = 'fetch-people';
export const FETCH_PERSON = 'fetch-person';
export const CREATE_PERSON = 'create-person';
export const CREATE_PERSON_ERROR = 'create-person-error';
export const UPDATE_PERSON = 'update-person';
export const DELETE_PERSON = 'delete-person';
export const UPDATE_PERSON_PASSWORD = 'update-person-password';
export const UPLOAD_PERSON_FILE = 'upload-person-file';
export const DELETE_PERSON_FILE = 'delete-person-file';

export const STORE_PEOPLE_SELECTED_COLUMNS = 'store-people-selected-columns';

//projects
export const FETCH_PROJECTS = 'fetch-projects';
export const FETCH_PROJECT = 'fetch-project';
export const CREATE_PROJECT = 'create-project';
export const UPDATE_PROJECT = 'update-project';
export const DELETE_PROJECT = 'delete-project';
export const UPLOAD_PROJECT_FILE = 'upload-project-file';
export const DELETE_PROJECT_FILE = 'delete-project-file';

//search
export const FETCH_ALL_SEARCH = 'fetch-all-search';
export const SET_SEARCH_TERM = 'set-search-term';
export const SEARCH_ALL_CLEAR = 'search-all-clear';

//alerts
export const CREATE_ALERT = 'create-alert';
export const DELETE_ALERT = 'delete-alert';
export const DELETE_ALERTS_ALL = 'delete-alerts-all';
