import axios from 'axios';
import { change, untouch } from 'redux-form';
import {
  FETCH_PROFILE,
  FETCH_PEOPLE,
  FETCH_PERSON,
  UPDATE_PERSON,
  DELETE_PERSON,
  UPLOAD_PERSON_FILE,
  DELETE_PERSON_FILE,
  FETCH_ALL_SEARCH,
  SET_SEARCH_TERM,
  SEARCH_ALL_CLEAR,
  LOGOUT_LOCAL,
  CREATE_ALERT,
  DELETE_ALERT,
  DELETE_ALERTS_ALL,
  CREATE_PERSON,
  STORE_PEOPLE_SELECTED_COLUMNS,
  FETCH_PROJECTS,
  CREATE_PROJECT,
  FETCH_PROJECT,
  UPDATE_PROJECT,
  DELETE_PROJECT,
  UPLOAD_PROJECT_FILE,
  DELETE_PROJECT_FILE,
  UPDATE_TABLE_CONFIGURATION
} from './types';

const API1 = '/api/1';

export const updateTableConfiguration = (tableName, configuration) => async dispatch => {
  return dispatch({ type: UPDATE_TABLE_CONFIGURATION, tableName, configuration });
};

const putFileInStorage = (file, url, onSuccess) => async dispatch => {
  const options = {
    headers: {
      'Content-Type': file.type
    }
  };

  await axios
    .put(url, file, options)
    .then(result => {
      onSuccess();
      dispatch(createAlert({ success: 'File Uploaded!' }));
    })
    .catch(err => {
      return dispatch(
        createAlert({ error: 'An error occured while uploading the file(s).' + err })
      );
    });
};

export const loginLocal = values => async dispatch => {
  await axios
    .post(`${API1}/auth/login`, values)
    .then(() => {
      dispatch(createAlert({ success: 'Welcome!' }));
      dispatch(fetchProfile());
    })
    .catch(err => {
      //clear the password
      dispatch(change('LoginLocalForm', 'password', null));
      dispatch(untouch('LoginLocalForm', 'password'));

      const status = err.response.status;
      const description = err.response.data.description;
      const lockedUntil = err.response.data.lockedUntil;

      if (status && lockedUntil) {
        return dispatch(createAlert({ error: 'Try again in a few minutes...' }));
      } else if (status && description) {
        return dispatch(createAlert({ error: description }));
      } else {
        return dispatch(createAlert({ error: 'Unknown Error Occured.' }));
      }
    });
};

export const logoutLocal = () => async dispatch => {
  const res = await axios.post(`${API1}/auth/logout`);

  dispatch({ type: LOGOUT_LOCAL, payload: res.data });
};

export function setSearchTerm(term) {
  return {
    type: SET_SEARCH_TERM,
    term
  };
}

export function fetchSearch(term) {
  if (!term) {
    return {
      type: FETCH_ALL_SEARCH,
      payload: { data: { people: [] } }
    };
  }

  const request = axios.get(`${API1}/search/all?term=${term}`);
  return {
    type: FETCH_ALL_SEARCH,
    payload: request
  };
}

export function searchAllClear() {
  return {
    type: SEARCH_ALL_CLEAR
  };
}

//User

export const fetchProfile = () => async dispatch => {
  const res = await axios.get(`${API1}/auth/profile`);

  dispatch({ type: FETCH_PROFILE, payload: res.data });
};

export const forgotPasswordReset = (id, values, callback) => async dispatch => {
  await axios
    .post(`${API1}/auth/forgotpwreset/${id}`, values)
    .then(() => {
      dispatch(
        createAlert({
          success: 'Password was updated. Please Login.'
        })
      );
      return callback();
    })
    .catch(err => {
      if (err.response) {
        const status = err.response.status;
        const description = err.response.data.description;
        if (status === 400 && description) {
          return dispatch(createAlert({ error: description }));
        } else if (status === 404) {
          return dispatch(
            createAlert({
              error:
                'Reset link has been used, link has expired, or person with that email address was not found.'
            })
          );
        }
      }
      return dispatch(createAlert({ error: 'An error occured while updating the password.' }));
    });
};

export const forgotPasswordRequest = (values, callback) => async dispatch => {
  await axios
    .post(`${API1}/auth/forgotpwrequest`, values)
    .then(() => {
      dispatch(
        createAlert({
          success:
            'A password reset link has been sent to your email address. You should receive this link within a few minutes if you have an account.'
        })
      );
      return callback();
    })
    .catch(err => {
      if (err.response) {
        const status = err.response.status;
        if (status === 400) {
          return dispatch(
            createAlert({ error: 'reCAPTCHA was not complete or was expired. Please try again.' })
          );
        }
      }
      return createAlert({ error: 'Unknown error occured.' });
    });
};

//Projects

export const fetchProjects = () => async dispatch => {
  const res = await axios.get(`${API1}/projects`);

  dispatch({ type: FETCH_PROJECTS, payload: res.data });
};

export const createProject = (values, callback) => async dispatch => {
  const res = await axios
    .post(`${API1}/projects`, values)
    .then(() => {
      dispatch(createAlert({ success: `${values.name} added with ID ${values.id}` }));
      return callback();
    })
    .catch(err => {
      if (err.response) {
        const status = err.response.status;
        if (status === 409) {
          return dispatch(
            createAlert({ error: `Project already exists with the ID ${values.id}` })
          );
        }
      }
      return dispatch(createAlert({ error: 'Unknown error occured.' }));
    });

  dispatch({ type: CREATE_PROJECT, payload: res });
};

export const fetchProject = id => async dispatch => {
  const res = await axios.get(`${API1}/projects/${id}`);

  dispatch({ type: FETCH_PROJECT, payload: res.data });
};

export const updateProject = (id, values) => async dispatch => {
  await axios
    .put(`${API1}/projects/${id}`, values)
    .then(res => {
      dispatch(createAlert({ success: 'Project updated!' }));
      return dispatch({ type: UPDATE_PROJECT, payload: res.data });
    })
    .catch(err => {
      if (err.response) {
        const status = err.response.status;
        if (status === 409) {
          return dispatch(createAlert({ error: `Project already exists with the same ID.` }));
        } else if (status === 404) {
          return dispatch(createAlert({ error: 'Project not found.' }));
        }
      }
      return dispatch(createAlert({ error: 'An error occured while updating the project.' }));
    });
};

export const deleteProject = (id, callback) => async dispatch => {
  const res = await axios.delete(`${API1}/projects/${id}`).then(() => callback());

  dispatch({ type: DELETE_PROJECT, payload: res.data });
};

export const uploadProjectFile = (id, file) => async dispatch => {
  const { name, type } = file;
  await axios
    .get(`/api/1/projects/${id}/files/put?name=${name}&type=${type}`)
    .then(res => {
      const {
        data: { url }
      } = res;

      const onSuccess = () => {
        dispatch(putProjectFile(id, name, file.size));
      };

      return dispatch(putFileInStorage(file, url, onSuccess));
    })
    .catch(reason => {
      return dispatch(createAlert({ error: `Error uploading file.` }));
    });
};

export const putProjectFile = (id, name, size) => async dispatch => {
  const res = await axios.put(`${API1}/projects/${id}/files`, { name, size });

  dispatch({ type: UPLOAD_PROJECT_FILE, payload: res.data });
};

export const getProjectFile = (id, fileId) => async dispatch => {
  const tab = window.open('', '_blank');
  await axios.get(`${API1}/projects/${id}/files/${fileId}`).then(res => {
    const { data } = res;
    tab.location.href = data.url;
  });
};

export const deleteProjectFile = (id, fileId) => async dispatch => {
  const res = await axios.delete(`${API1}/projects/${id}/files/${fileId}`);

  dispatch({ type: DELETE_PROJECT_FILE, payload: res.data });
};

//People

export const fetchPeople = () => async dispatch => {
  const res = await axios.get(`${API1}/people`);

  dispatch({ type: FETCH_PEOPLE, payload: res.data });
};

export const createPerson = (values, callback) => async dispatch => {
  const res = await axios
    .post(`${API1}/people`, values)
    .then(() => {
      dispatch(createAlert({ success: `${values.name} added with ID ${values.id}` }));
      return callback();
    })
    .catch(err => {
      if (err.response) {
        const status = err.response.status;
        if (status === 409) {
          return dispatch(createAlert({ error: `Person already exists with the ID ${values.id}` }));
        }
      }
      return dispatch(createAlert({ error: 'Unknown error occured.' }));
    });

  dispatch({ type: CREATE_PERSON, payload: res });
};

export const fetchPerson = id => async dispatch => {
  const res = await axios.get(`${API1}/people/${id}`);

  dispatch({ type: FETCH_PERSON, payload: res.data });
};

export const updatePerson = (id, values) => async dispatch => {
  await axios
    .put(`${API1}/people/${id}`, values)
    .then(res => {
      dispatch(createAlert({ success: 'Person updated!' }));
      return dispatch({ type: UPDATE_PERSON, payload: res.data });
    })
    .catch(err => {
      if (err.response) {
        const status = err.response.status;
        if (status === 409) {
          return dispatch(
            createAlert({ error: `Person already exists with the same ID or Email Address` })
          );
        } else if (status === 404) {
          return dispatch(createAlert({ error: 'Person not found.' }));
        }
      }
      return dispatch(createAlert({ error: 'An error occured while updating the person.' }));
    });
};

export const deletePerson = (id, callback) => async dispatch => {
  const res = await axios.delete(`${API1}/people/${id}`).then(() => callback());

  dispatch({ type: DELETE_PERSON, payload: res.data });
};

export const uploadPersonFile = (id, file) => async dispatch => {
  const { name, type } = file;
  await axios
    .get(`/api/1/people/${id}/files/put?name=${name}&type=${type}`)
    .then(res => {
      const {
        data: { url }
      } = res;

      const onSuccess = () => {
        dispatch(putPersonFile(id, name, file.size));
      };

      return dispatch(putFileInStorage(file, url, onSuccess));
    })
    .catch(reason => {
      return dispatch(createAlert({ error: `Error uploading file.` }));
    });
};

export const putPersonFile = (id, name, size) => async dispatch => {
  const res = await axios.put(`${API1}/people/${id}/files`, { name, size });

  dispatch({ type: UPLOAD_PERSON_FILE, payload: res.data });
};

export const getPersonFile = (id, fileId) => async dispatch => {
  const tab = window.open('', '_blank');
  await axios.get(`${API1}/people/${id}/files/${fileId}`).then(res => {
    const { data } = res;
    tab.location.href = data.url;
  });
};

export const deletePersonFile = (id, fileId) => async dispatch => {
  const res = await axios.delete(`${API1}/people/${id}/files/${fileId}`);

  dispatch({ type: DELETE_PERSON_FILE, payload: res.data });
};

export const updatePersonPassword = (id, values, callback) => async dispatch => {
  await axios
    .put(`${API1}/people/${id}/password`, values)
    .then(() => {
      dispatch(createAlert({ success: 'Password Updated!' }));
      callback();
    })
    .catch(err => {
      if (err.response) {
        const status = err.response.status;
        const description = err.response.data.description;
        if (status === 400 && description) {
          return dispatch(createAlert({ error: description }));
        } else if (status === 404) {
          return dispatch(createAlert({ error: 'Person not found.' }));
        }
      }
      return dispatch(createAlert({ error: 'An error occured while updating the password.' }));
    });

  //dispatch({ type: UPDATE_PERSON_PASSWORD, payload: res.data });
};

export function createAlert(alert) {
  return { type: CREATE_ALERT, alert };
}

export function deleteAlert(alert) {
  return { type: DELETE_ALERT, alert };
}
export function deleteAlertsAll() {
  return { type: DELETE_ALERTS_ALL };
}

export function storePeopleSelectedColumns(selectedColumns) {
  return { type: STORE_PEOPLE_SELECTED_COLUMNS, selectedColumns };
}
