import React, { Component } from 'react';
import { connect } from 'react-redux';

import Alert from './Alert';

class Alerts extends Component {
  renderAlerts(alerts) {
    return alerts.map((alert, index) => {
      return <Alert key={index} alert={alert} />;
    });
  }

  render() {
    const { alerts } = this.props;

    if (!alerts) return null;
    return (
      <div className="row fixed-top fixed-top-2">
        <div className="col">{this.renderAlerts(alerts)}</div>
      </div>
    );
  }
}

function mapStateToProps({ alerts }) {
  return {
    alerts
  };
}

export default connect(mapStateToProps)(Alerts);
