import _ from 'lodash';
import React from 'react';
import { Field, FormSection } from 'redux-form';

import FieldGroup from './FieldGroup';
import FieldColumn from './FieldColumn';
import InputField from './inputs/InputField';

class SubRecord extends FormSection {
  componentDidMount() {
    this.renderStandardField = this.renderStandardField.bind(this);
  }

  renderInputField(field) {
    return <InputField field={field} />;
  }

  renderStandardField(identifier) {
    const {
      config: { standardFields },
      dateFormat
    } = this.props;

    if (standardFields[identifier]) {
      const field = standardFields[identifier];
      return (
        <Field
          {...field}
          key={identifier}
          label={field.name}
          name={identifier}
          dateFormat={dateFormat}
          type={field.kind}
          component={this.renderInputField}
        />
      );
    }
  }

  renderItems(items) {
    return _.map(items, item => {
      return this.renderStandardField(item.standardField);
    });
  }

  renderColumns(columns) {
    const width = columns.length > 1 ? 'half' : null;

    return _.map(columns, column => {
      const items = this.renderItems(column.items);
      return (
        <FieldColumn key={column._id} width={width}>
          {items}
        </FieldColumn>
      );
    });
  }

  renderGroups() {
    const {
      config: {
        view: { groups }
      }
    } = this.props;
    return _.map(groups, group => {
      const columns = this.renderColumns(group.columns);
      return (
        <FieldGroup key={group._id} id={group._id} name={group.name}>
          {columns}
        </FieldGroup>
      );
    });
  }

  render() {
    return (
      <div className="row">
        <h5 className="col-12">{this.props.title}</h5>
        <div className="col-12">{this.renderGroups()}</div>
      </div>
    );
  }
}
export default SubRecord;
