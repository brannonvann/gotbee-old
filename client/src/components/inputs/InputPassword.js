import React from 'react';

const InputLine = field => {
  const {
    meta: { touched, error }
  } = field;

  return (
    <input
      {...field.input}
      type="password"
      className={'form-control form-control-sm ' + (touched && error ? 'is-invalid' : '')}
    />
  );
};

export default InputLine;
