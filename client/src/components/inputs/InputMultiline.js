import React from 'react';

const InputMultiline = field => {
  const {
    meta: { touched, error }
  } = field;

  return (
    <textarea
      {...field.input}
      className={'form-control form-control-sm ' + (touched && error ? 'is-invalid' : '')}
      rows="15"
      cols="40"
    />
  );
};

export default InputMultiline;
