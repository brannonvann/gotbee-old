import _ from 'lodash';
import React from 'react';
import Select from 'react-select';

const InputSelect = field => {
  const options = field.labelValues
    ? field.labelValues
    : _.map(field.options, function(v) {
        return { label: v.label || v, value: v.value || v };
      });

  //console.log('options', options);

  const {
    meta: { touched, error }
  } = field;

  return (
    <Select
      {...field.input}
      id={field.name}
      onBlurResetsInput={false}
      onSelectResetsInput={false}
      multiple={true}
      options={options}
      simpleValue
      clearable={true}
      searchable={true}
      onBlur={() => field.input.onBlur(field.value)}
      className={touched && error ? 'is-invalid' : ''}
    />
  );
};

export default InputSelect;
