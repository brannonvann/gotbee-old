import React from 'react';

const InputEmail = field => {
  const {
    meta: { touched, error }
  } = field;

  return (
    <input
      className={'form-control form-control-sm ' + (touched && error ? 'is-invalid' : '')}
      type="email"
      {...field.input}
    />
  );
};

export default InputEmail;
