import React, { Component } from 'react';

import InputLine from './InputLine';
import InputPassword from './InputPassword';
import InputCheckbox from './InputCheckbox';
import InputNumber from './InputNumber';
import InputDate from './InputDate';
import InputEmail from './InputEmail';
import InputSelect from './InputSelect';
import InputMultiline from './InputMultiline';
import InputRecordSelect from './InputRecordSelect';

class InputField extends Component {
  render() {
    const { field } = this.props;
    const {
      meta: { touched, error }
    } = field;

    let input = null;

    if (field.kind === 'multiline') {
      input = InputMultiline(field);
    } else if (field.kind === 'line') {
      input = InputLine(field);
    } else if (field.kind === 'password') {
      input = InputPassword(field);
    } else if (field.kind === 'email') {
      input = InputEmail(field);
    } else if (field.kind === 'number') {
      input = InputNumber(field);
    } else if (field.kind === 'date') {
      input = InputDate(field);
    } else if (field.kind === 'checkbox') {
      input = InputCheckbox(field);
    } else if (field.kind === 'select') {
      input = InputSelect(field);
    } else if (field.kind === 'record') {
      input = <InputRecordSelect {...field} />;
    } else {
      return null;
    }

    return (
      <div className="form-group">
        <label>{field.label}</label>
        {input}
        <div className="text-danger">{touched ? error : ''}</div>
      </div>
    );
  }
}

export default InputField;
