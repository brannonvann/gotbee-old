import _ from 'lodash';
import { Component } from 'react';

import { connect } from 'react-redux';

import InputSelect from './InputSelect';

class InputRecordSelect extends Component {
  render() {
    const { records, input, name, meta } = this.props;
    const props = { input, name, meta };

    /*
    props.labelValues = _.map(records, r => {
      if (!r.active) {
        return null;
      }
      return { label: r.name, value: r._id };
    });
    */
    props.labelValues = [];
    _.forEach(records, function(r) {
      if (r.active) {
        props.labelValues.push({ label: r.name, value: r._id });
        return;
      }
    });
    console.log(props.labelValues);

    return InputSelect(props);
  }
}

function mapStateToProps(state, ownProps) {
  return {
    records: state[ownProps.record]
  };
}

InputRecordSelect = connect(
  mapStateToProps,
  {}
)(InputRecordSelect);

export default InputRecordSelect;
