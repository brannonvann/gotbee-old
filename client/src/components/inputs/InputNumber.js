import React from 'react';

const InputNumber = field => {
  const {
    meta: { touched, error }
  } = field;
  return (
    <input
      className={'form-control form-control-sm ' + (touched && error ? 'is-invalid' : '')}
      type="number"
      {...field.input}
    />
  );
};

export default InputNumber;
