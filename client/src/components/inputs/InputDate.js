import React from 'react';
import DateTimePicker from 'react-widgets/lib/DateTimePicker';
import moment from 'moment';
import momentLocaliser from 'react-widgets-moment';

momentLocaliser(moment);

const InputDate = ({ input: { onChange, value }, dateFormat }) => (
  <DateTimePicker
    onChange={onChange}
    format={dateFormat}
    time={false}
    value={!value ? null : new Date(value)}
  />
);

/*field => {
    return <input className="form-control form-control-sm" {...field} />;
  
    
  };
  */

export default InputDate;
