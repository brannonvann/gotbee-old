import React from 'react';

const InputCheckbox = ({ input, options, meta, type }) => {
  return (
    <input
      {...input}
      {...options}
      active={meta.active.toString()}
      type="checkbox"
      className="form-control"
    />
  );
};

export default InputCheckbox;
