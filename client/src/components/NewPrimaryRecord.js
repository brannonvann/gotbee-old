import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import InputField from './inputs/InputField';
import FormContainerSmall from './FormContainerSmall';

class NewMainRecord extends Component {
  constructor(props) {
    super(props);
    this.state = { createAnother: false };

    this.handleCreateAnotherInputChange = this.handleCreateAnotherInputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  handleCreateAnotherInputChange({ target: { checked } }) {
    this.setState({
      createAnother: checked
    });
  }

  renderField(field) {
    return <InputField field={field} />;
  }

  onSubmit(values) {
    const { reset, history, createWith, redirectToAfterCreate } = this.props;
    const { createAnother } = this.state;

    console.log('redirectToAfterCreate', redirectToAfterCreate);

    createWith(values, () => {
      reset();
      if (!createAnother) history.push(redirectToAfterCreate);
    });
  }

  render() {
    const { handleSubmit, pristine, submitting, invalid, title } = this.props;
    return (
      <FormContainerSmall>
        <form action="" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
          <h5>{title}</h5>
          <Field autofocus label="Name" name="name" kind="line" component={this.renderField} />
          <Field label="ID" name="id" kind="line" component={this.renderField} />
          <label>Create Another?</label>
          <input
            type="checkbox"
            className="ml-1"
            checked={this.state.createAnother}
            onChange={this.handleCreateAnotherInputChange}
          />
          <button
            className="btn btn-primary d-block"
            type="submit"
            disabled={pristine || submitting || invalid}
          >
            Submit
          </button>
        </form>
      </FormContainerSmall>
    );
  }
}

function validate(values) {
  //example: console.log(values) -> {title: "asdf", categories: "asdf", content: "asdf"}
  const errors = {};

  //validate the inputs from 'values'
  if (!values.name) {
    errors.name = 'Enter a name!';
  }

  if (!values.id) {
    errors.id = 'Enter an id!';
  }
  return errors;
}

export default reduxForm({
  validate,
  form: 'NewMainRecordForm'
})(
  connect(
    null,
    {}
  )(withRouter(NewMainRecord))
);
