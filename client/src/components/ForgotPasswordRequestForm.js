import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Redirect } from 'react-router-dom';

import { connect } from 'react-redux';
import ReCAPTCHA from 'react-google-recaptcha';

import { forgotPasswordRequest } from '../actions';
import InputField from './inputs/InputField';

class ForgotPasswordRequestForm extends Component {
  constructor(props) {
    super(props);

    this.state = { success: false };
  }

  renderField(field) {
    return <InputField field={field} />;
  }

  onSubmit(values) {
    this.props.forgotPasswordRequest(values, () => {
      this.setState({ success: true });
    });
  }

  renderRecaptcha(field) {
    const {
      meta: { touched, error }
    } = field;
    return (
      <div className="form-group">
        <ReCAPTCHA
          sitekey={process.env.REACT_APP_RECAPTCHA_SITE_KEY}
          onChange={field.input.onChange}
        />
        <div className="text-danger">{touched ? error : ''}</div>
      </div>
    );
  }

  render() {
    const { handleSubmit, pristine, submitting, invalid } = this.props;
    const { success } = this.state;

    if (success) return <Redirect to="/login" />;

    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
          <p>
            Please enter your email address below. A new password will be sent to your email address
            containing a temporary password.
          </p>
          <Field label="Email" name="email" kind="email" component={this.renderField} />
          <Field name="g-recaptcha-response" component={this.renderRecaptcha} />
          <button
            className="btn btn-primary"
            type="submit"
            disabled={pristine || submitting || invalid}
          >
            Request Reset Link
          </button>
          {this.props.children}
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (!values.email) {
    errors.email = 'Enter your email address!';
  }

  if (!values['g-recaptcha-response']) {
    errors['g-recaptcha-response'] = 'Please complete the reCAPTCHA!';
  }
  return errors;
}

export default reduxForm({
  validate,
  form: 'ForgotPasswordRequestForm' //think of as the name of the form. needs to be unique!
})(
  connect(
    null,
    { forgotPasswordRequest }
  )(ForgotPasswordRequestForm)
);
