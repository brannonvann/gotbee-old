import React from 'react';

const RecordNavigatorShell = props => {
  return <li className="nav-item text-left">{props.children}</li>;
};

export default RecordNavigatorShell;
