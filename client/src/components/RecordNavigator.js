import React from 'react';

const RecordNavigator = props => {
  return <ul className="nav nav-pills flex-column">{props.children}</ul>;
};

export default RecordNavigator;
