import React from 'react';

const PasswordRules = () => (
  <div className="text-muted mt-3">
    The password must:
    <ul>
      <li>be at least 10 characters long </li>
      <li>have at least one lowercase letter</li>
      <li>have at least one uppercase letter</li>
      <li>have at least one special character</li>
    </ul>
  </div>
);

export default PasswordRules;
