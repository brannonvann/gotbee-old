import React from 'react';
import RecordNavigatorShell from './RecordNavigatorShell';

const RecordNavigatorItem = ({ id, name }) => {
  return (
    <RecordNavigatorShell>
      <a className="nav-link text-truncate" title={name} href={`#${id}`}>
        {name}
      </a>
    </RecordNavigatorShell>
  );
};

export default RecordNavigatorItem;
