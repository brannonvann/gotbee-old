import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { updateTableConfiguration } from '../actions';

import ReactTable from 'react-table';

class TableTool extends Component {
  constructor(props) {
    super(props);

    this.onPageChange = this.onPageChange.bind(this);
    this.onPageSizeChange = this.onPageSizeChange.bind(this);
    this.onFilteredChange = this.onFilteredChange.bind(this);
    this.onSortedChange = this.onSortedChange.bind(this);
    this.onExpandedChange = this.onExpandedChange.bind(this);
    this.onResizeChange = this.onResizeChange.bind(this);
    this.resolveData = this.resolveData.bind(this);
  }

  onColumnSelectionChange(selectedColumns) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { selectedColumns });
    }
  }

  onPageChange(pageIndex) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { pageIndex });
    }
  }

  onPageSizeChange(pageSize, pageIndex) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { pageSize, pageIndex });
    }
  }

  onFilteredChange(filtered, column) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { filtered, column, pageIndex: 0 });
    }
  }

  onSortedChange(newSorted, column, shiftKey) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { newSorted, column, shiftKey });
    }
  }

  onExpandedChange(newExpanded, index, event) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { newExpanded, index, event });
    }
  }

  onResizeChange(newResized, event) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { newResized, event });
    }
  }

  resolveData(data) {
    const {
      config: { standardFields }
    } = this.props;

    const dataCopy = JSON.parse(JSON.stringify(data));

    dataCopy.map(row => {
      //for each column
      for (const key in row) {
        if (
          row.hasOwnProperty(key) &&
          standardFields[key] &&
          standardFields[key].kind === 'record'
        ) {
          const _id = row[key];
          const standardField = standardFields[key];
          const record = standardField.record;

          row[key] =
            this.props[record] && this.props[record][_id] ? this.props[record][_id].name : '';
        }
      }
      return row;
    });
    return dataCopy;
  }

  getColumns() {
    const {
      dateFormat,
      config: {
        standardFields,
        view: {
          table: { columns }
        }
      }
    } = this.props;

    const tableColumns = [];

    _.map(columns, column => {
      const tableColumn = {};
      const standardField = standardFields[column];

      tableColumn.Header = standardField.name;

      if (standardFields[column].kind === 'date') {
        tableColumn.id = standardField._id;
        tableColumn.accessor = d => {
          return d[standardField._id]
            ? moment(d[standardField._id])
                .local()
                .format(dateFormat)
            : null;
        };
        //} else if (standardFields[column].kind === 'record') {
        //  tableColumn.accessor = `${standardField._id}.name`;
      } else {
        tableColumn.accessor = standardField._id;
      }

      tableColumns.push(tableColumn);
    });
    return tableColumns;
  }

  render() {
    const { data, tableName, tables, defaultPageSize, onRowClick } = this.props;
    const tableConfig = tables[tableName] || {};
    const { pageIndex, pageSize, newSorted, newExpanded, filtered } = tableConfig;

    const columns = this.props.columns || this.getColumns();

    return (
      <ReactTable
        data={this.resolveData(data)}
        columns={columns}
        defaultPageSize={defaultPageSize}
        page={pageIndex}
        pageSize={pageSize}
        sorted={newSorted}
        expanded={newExpanded}
        minRows={1}
        noDataText={'No Records'}
        filtered={filtered}
        className="-striped -highlight"
        filterable
        getTrProps={(state, rowInfo, column, instance) => ({
          onClick: event => {
            onRowClick(rowInfo);
            //console.log('onRowClick-->Event', event);
            //console.log('onRowClick-->rowInfo', rowInfo);
            //console.log('onRowClick-->state', state);
            //console.log('onRowClick-->column', column);
            //console.log('onRowClick-->instance', instance);
            //this.props.history.push(`${''}/${rowInfo.original._id}`);
          }
        })}
        onPageChange={this.onPageChange} // pageIndex
        onPageSizeChange={this.onPageSizeChange} // pageSize, pageIndex
        onFilteredChange={this.onFilteredChange} // filtered, column
        onSortedChange={this.onSortedChange} // newSorted, column, shiftKey
        onExpandedChange={this.onExpandedChange} // newExpanded, index, event
        onResizeChange={this.onResizeChange} // newResized, event
      />
    );
  }
}

function mapStateToProps(state, ownProps) {
  const { tables } = state;
  const props = { tables };
  const {
    config: { standardFields }
  } = ownProps;
  if (
    ownProps &&
    ownProps.config &&
    ownProps.config.view &&
    ownProps.config.view.table &&
    ownProps.config.view.table.columns
  ) {
    const columns = ownProps.config.view.table.columns;
    columns.map(column_id => {
      const standardField = standardFields[column_id];

      if (standardField.kind === 'record') {
        const record = standardField.record;
        props[record] = state[record];
      }
      return null;
    });
  }

  return props;
}

export default connect(
  mapStateToProps,
  { updateTableConfiguration }
)(TableTool);
