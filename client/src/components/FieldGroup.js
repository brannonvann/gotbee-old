import React, { Component } from 'react';

class FieldGroup extends Component {
  render() {
    const { name, id } = this.props;

    return (
      <section id={id} className="row field-group">
        <h4 className="col-12 text-secondary">{name}</h4>
        {this.props.children}
      </section>
    );
  }
}

export default FieldGroup;
