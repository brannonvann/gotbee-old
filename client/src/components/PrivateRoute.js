import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { fetchPeople, fetchProjects } from '../actions';

class PrivateRoute extends Component {
  componentDidMount() {
    this.props.fetchPeople();
    this.props.fetchProjects();
  }

  render() {
    const { component: Component, ...rest } = this.props;
    if (this.props.auth.profile === false) {
      return <Redirect to={{ pathname: '/login', state: { from: this.props.location } }} />;
    } else if (!this.props.auth.profile) {
      return <div />;
    } else if (!this.props.auth.profile.tenant) {
      return (
        <div>
          <p>Tenant configuration is not complete. Please contact Gotbee support.</p>
        </div>
      );
    } else {
      return <Route {...rest} render={props => <div>{<Component {...props} />}</div>} />;
    }
  }
}

function mapStateToProps({ auth }) {
  return {
    auth: auth
  };
}

export default connect(
  mapStateToProps,
  {
    fetchPeople,
    fetchProjects
  }
)(PrivateRoute);
