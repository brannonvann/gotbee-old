import _ from 'lodash';
import matchSorter from 'match-sorter';
import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import Select from 'react-select';
import moment from 'moment';
import { updateTableConfiguration } from '../actions';

import ReactTable from 'react-table';

class PrimaryRecords extends Component {
  constructor(props) {
    super(props);
    this.state = { customFields: {} };

    this.onColumnSelectionChange = this.onColumnSelectionChange.bind(this);
    this.getColumns = this.getColumns.bind(this);
    this.onPageChange = this.onPageChange.bind(this);
    this.onPageSizeChange = this.onPageSizeChange.bind(this);
    this.onFilteredChange = this.onFilteredChange.bind(this);
    this.onSortedChange = this.onSortedChange.bind(this);
    this.onExpandedChange = this.onExpandedChange.bind(this);
    this.onResizeChange = this.onResizeChange.bind(this);
  }

  componentWillMount() {
    const customFields = _.mapKeys(this.props.customFields, '_id');
    this.setState({ customFields });
  }

  componentDidMount() {
    this.props.fetchRecords();
  }

  getColumns() {
    const { customFields } = this.state;
    const {
      profile,
      config: { standardFields },
      defaultColumns,
      tables,
      tableName
    } = this.props;

    const {
      tenant: { dateFormat }
    } = profile;

    const table = tables[tableName];
    const selectedColumns = table && table.selectedColumns ? table.selectedColumns : defaultColumns;

    if (!selectedColumns) return [];

    return selectedColumns.split(',').map(_id => {
      let name;
      let kind;
      let accessor = _id;

      if (standardFields[_id]) {
        name = standardFields[_id].name;
        kind = standardFields[_id].kind;

        if (kind === 'date') {
          accessor = d => {
            return moment(d[_id])
              .local()
              .format(dateFormat);
          };
        } else if (kind === 'checkbox') {
          accessor = d => {
            return d[_id] ? 'Yes' : 'No';
          };
        }
      } else {
        accessor = 'customFields.' + _id;
        name = customFields[_id].name;
        kind = customFields[_id].kind;

        if (kind === 'date') {
          accessor = d => {
            return d.customFields && d.customFields[_id]
              ? moment(d.customFields[_id])
                  .local()
                  .format(dateFormat)
              : null;
          };
        } else if (kind === 'checkbox') {
          accessor = d => {
            return d.customFields && d.customFields[_id] ? 'Yes' : 'No';
          };
        }
      }

      return {
        id: _id,
        Header: name,
        accessor,
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: [_id] }),
        filterAll: true
      };
    });
  }

  renderColumnSelector() {
    const { customFields } = this.state;
    const {
      tables,
      config: { standardFields },
      tableName,
      defaultColumns
    } = this.props;
    const table = tables[tableName];
    const selectedColumns = table && table.selectedColumns ? table.selectedColumns : defaultColumns;

    const sfOptions = _.map(standardFields, v => ({ label: v.name, value: v._id }));
    const cfOptions = _.map(customFields, v => ({ label: v.name, value: v._id }));
    const options = [...sfOptions, ...cfOptions];

    options.sort(function(a, b) {
      const x = a.label.toLowerCase();
      const y = b.label.toLowerCase();
      if (x < y) return -1;
      if (x > y) return 1;
      return 0;
    });

    return (
      <Select
        id="selectedColumns"
        onBlurResetsInput={false}
        onSelectResetsInput={false}
        multi={true}
        options={options}
        simpleValue
        clearable={true}
        searchable={true}
        onChange={this.onColumnSelectionChange}
        value={selectedColumns}
      />
    );
  }

  onColumnSelectionChange(selectedColumns) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { selectedColumns });
    }
  }

  onPageChange(pageIndex) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { pageIndex });
    }
  }
  onPageSizeChange(pageSize, pageIndex) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { pageSize, pageIndex });
    }
  }
  onFilteredChange(filtered, column) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { filtered, column, pageIndex: 0 });
    }
  }

  onSortedChange(newSorted, column, shiftKey) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { newSorted, column, shiftKey });
    }
  }

  onExpandedChange(newExpanded, index, event) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { newExpanded, index, event });
    }
  }

  onResizeChange(newResized, event) {
    const { tableName, updateTableConfiguration } = this.props;
    if (tableName) {
      updateTableConfiguration(tableName, { newResized, event });
    }
  }

  render() {
    const { records, path, tableName, tables } = this.props;
    const tableConfig = tables[tableName] || {};
    const { pageIndex, pageSize, newSorted, newExpanded, filtered } = tableConfig;

    return (
      <div>
        {this.renderColumnSelector()}
        {/*<ul className="list-group">{this.renderPeople()}</ul> */}
        <ReactTable
          data={records}
          columns={this.getColumns()}
          defaultPageSize={10}
          page={pageIndex}
          pageSize={pageSize}
          sorted={newSorted}
          expanded={newExpanded}
          filtered={filtered}
          className="-striped -highlight"
          filterable
          getTrProps={(state, rowInfo, column, instance) => ({
            onClick: event => {
              this.props.history.push(`${path}/${rowInfo.original._id}`);
            }
          })}
          onPageChange={this.onPageChange} // pageIndex
          onPageSizeChange={this.onPageSizeChange} // pageSize, pageIndex
          onFilteredChange={this.onFilteredChange} // filtered, column
          onSortedChange={this.onSortedChange} // newSorted, column, shiftKey
          onExpandedChange={this.onExpandedChange} // newExpanded, index, event
          onResizeChange={this.onResizeChange} // newResized, event
        />
      </div>
    );
  }
}

function mapStateToProps({ auth, tables }) {
  return {
    profile: auth.profile,
    tables
  };
}

export default connect(
  mapStateToProps,
  {
    updateTableConfiguration
  }
)(withRouter(PrimaryRecords));
