import _ from 'lodash';
import React, { Component, Fragment } from 'react';
import Modal from 'react-modal';

import TableTool from './TableTool';
import SubRecord from './SubRecord';

class SubRecords extends Component {
  constructor() {
    super();

    Modal.setAppElement('#modal-root');

    this.state = {
      selectedIndex: null,
      modalIsOpen: false
    };

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.addRecord = this.addRecord.bind(this);
    this.removeRecord = this.removeRecord.bind(this);
  }

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    //this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  addRecord() {
    const { fields } = this.props;

    fields.push({ _: null }); //invalid value to represent record. react-table requires some value.
    this.setState({ selectedIndex: fields.length });
    this.openModal();
  }

  removeRecord() {
    const { fields } = this.props;
    fields.remove(this.state.selectedIndex);
    this.setState({ selectedIndex: null });
    this.closeModal();
  }

  render() {
    const { selectedIndex } = this.state;
    const { fields, title, config, dateFormat, toolTitle } = this.props;
    const records = this.props.fields.getAll();
    const customStyles = {
      overlay: {
        zIndex: 1040
      },
      content: {
        zIndex: 1050,
        border: 'none',
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
      }
    };

    return (
      <Fragment>
        <label className="mr-2">{toolTitle}</label>
        <div className="d-inline">
          <button
            type="button"
            className="btn btn-sm btn-outline-success "
            onClick={this.addRecord}
          >
            <span className="oi oi-plus" /> Add
          </button>
        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel={title}
        >
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{`Edit ${title}`}</h5>
              <button type="button" className="close" onClick={this.closeModal}>
                <span>&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="container-fluid">
                <div className="col-12">
                  <SubRecord
                    config={config}
                    dateFormat={dateFormat}
                    name={`${fields.name}[${selectedIndex}]`}
                  />
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" onClick={this.closeModal}>
                Close
              </button>
              <button type="button" className="btn btn-danger" onClick={this.removeRecord}>
                <span className="oi oi-trash" /> Remove
              </button>
            </div>
          </div>
        </Modal>
        <TableTool
          data={records}
          dateFormat={dateFormat}
          defaultPageSize={Math.min(5, records.length || 1)}
          //title="title"
          onRowClick={rowInfo => {
            this.openModal();
            const _id = rowInfo.original._id;
            const selectedIndex = _.findIndex(records, { _id });
            this.setState({ selectedIndex });
          }}
          config={config}
          tableName={fields.name} //-${id} adding id will save config for each record as it's own.
        />
      </Fragment>
    );
  }
}

export default SubRecords;
