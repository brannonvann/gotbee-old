import React from 'react';
import { Link } from 'react-router-dom';

const UniversalSearchListItem = ({ name, type, id, handleOnClick }) => {
  return (
    <Link
      onClick={handleOnClick}
      to={`/${type}/${id}`}
      className="list-group-item list-group-item-action"
    >
      {name}
    </Link>
  );
};
//<Link to={`/${type}/${id}`}>{name}</Link>
//<option>{name}</option>;

export default UniversalSearchListItem;
