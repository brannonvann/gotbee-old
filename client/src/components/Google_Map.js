import React, { Component } from 'react';

class GoogleMap extends Component {
  componentDidMount() {
    this.loadMap();
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.latitude !== prevProps.latitude ||
      this.props.longitude !== prevProps.longitude
    ) {
      this.loadMap();
    }
  }

  loadMap() {
    const { latitude, longitude } = this.props;
    const google = window.google;

    if (!(latitude && longitude)) {
      return;
    }

    const center = { lat: latitude, lng: longitude };

    //console.log('center', center);

    const map = new google.maps.Map(this.refs.map, {
      scrollwheel: false,
      zoom: 10,
      center
    });

    new google.maps.Marker({
      position: center,
      map: map,
      icon: '/open-iconic/png/person-3x.png'
    });
  }

  render() {
    return <div ref="map" className="map-record" />;
  }
}

export default GoogleMap;
