import _ from 'lodash';
import React, { Component } from 'react';
import moment from 'moment';
import ReactMoment from 'react-moment';

import { Field, reduxForm, FieldArray } from 'redux-form';
import { connect } from 'react-redux';

import FieldGroup from '../components/FieldGroup';
import FieldColumn from '../components/FieldColumn';
import InputField from '../components/inputs/InputField';
import RecordNavigator from '../components/RecordNavigator';
import RecordNavigatorLinkItem from '../components/RecordNavigatorLinkItem';
import RecordNavigatorInfoItem from '../components/RecordNavigatorInfoItem';
import RecordNavigatorButtonItem from '../components/RecordNavigatorButtonItem';
import GoogleMap from '../components/Google_Map';
import FileExplorer from '../components/FileExplorer';
import SubRecords from './SubRecords';

class PrimaryRecord extends Component {
  componentDidMount() {}

  renderInputField(field) {
    return <InputField field={field} />;
  }

  renderCustomField(identifier) {
    const customField = this.customFields[identifier];
    const dateFormat = this.props.profile.tenant.dateFormat;
    return (
      <Field
        {...this.customFields[identifier]}
        key={identifier}
        type={customField.kind}
        name={`customFields.${identifier}`}
        label={customField.name}
        dateFormat={dateFormat}
        component={this.renderInputField}
      />
    );
  }

  renderStandardField(identifier) {
    const {
      config: { standardFields }
    } = this.props;

    if (standardFields[identifier]) {
      const field = standardFields[identifier];
      return (
        <Field
          {...field}
          key={identifier}
          label={field.name}
          name={field._id}
          type={field.kind}
          component={this.renderInputField}
        />
      );
    }
  }

  renderSubrecords(props) {
    return <SubRecords {...props} />;
  }

  renderTool(identifier) {
    const {
      record,
      uploadFileHandler,
      openFileHandler,
      deleteFileHandler,
      profile,
      config
    } = this.props;
    const { dateFormat } = profile.tenant;
    switch (identifier) {
      case 'map':
        if (record.latitude && record.longitude)
          return (
            <div className="form-group" key={identifier}>
              <GoogleMap latitude={record.latitude} longitude={record.longitude} />
            </div>
          );
        return null;

      case 'files':
        return (
          <FileExplorer
            key={identifier}
            files={record.files}
            uploadFileHandler={uploadFileHandler}
            openFileHandler={openFileHandler}
            deleteFileHandler={deleteFileHandler}
          />
        );

      case 'ratings':
        return (
          <div className="form-group" key={identifier}>
            <FieldArray
              toolTitle={'Ratings'}
              title={'Rating'}
              name={`ratings`}
              component={this.renderSubrecords}
              dateFormat={dateFormat}
              config={config.rating}
            />
          </div>
        );

      case 'injuries':
        return (
          <div className="form-group" key={identifier}>
            <FieldArray
              toolTitle={'Injuries'}
              title={'Injury'}
              name={`injuries`}
              component={this.renderSubrecords}
              dateFormat={dateFormat}
              config={config.injury}
            />
          </div>
        );
      case 'personAssignments':
        return (
          <div className="form-group" key={identifier}>
            <FieldArray
              toolTitle={'Person Assignments'}
              title={'Person Assignment'}
              name={`personAssignments`}
              component={this.renderSubrecords}
              dateFormat={dateFormat}
              config={config.personAssignments}
            />
          </div>
        );
      default:
        return null;
    }
  }

  renderItems(items) {
    return _.map(items, item => {
      if (item.tool) {
        return this.renderTool(item.tool);
      } else if (item.standardField) {
        return this.renderStandardField(item.standardField);
      } else if (item.customField) {
        return this.renderCustomField(item.customField);
      }
    });
  }

  renderColumns(columns) {
    const width = columns.length > 1 ? 'half' : null;

    return _.map(columns, column => {
      const items = this.renderItems(column.items);
      return (
        <FieldColumn key={column._id} width={width}>
          {items}
        </FieldColumn>
      );
    });
  }

  renderGroups() {
    const { view } = this.props;
    const { groups } = view;
    return _.map(groups, group => {
      const columns = this.renderColumns(group.columns);
      return (
        <FieldGroup key={group._id} id={group._id} name={group.name}>
          {columns}
        </FieldGroup>
      );
    });
  }

  renderNavigator() {
    const {
      record,
      pristine,
      submitting,
      invalid,
      profile,
      view: { groups }
    } = this.props;
    return (
      <RecordNavigator>
        {this.renderRecordNavigatorLinkItems(groups)}
        <RecordNavigatorInfoItem
          key="createdAt"
          title={moment(record.createdAt).format(profile.tenant.dateFormat + ', h:mm:ss a')}
        >
          <span>Created: </span>
          <ReactMoment interval={30000} fromNow>
            {record.createdAt}
          </ReactMoment>
        </RecordNavigatorInfoItem>
        <RecordNavigatorInfoItem
          key="updatedAt"
          title={moment(record.updatedAt).format(profile.tenant.dateFormat + ', h:mm:ss a')}
        >
          <span>Updated: </span>
          <ReactMoment interval={30000} fromNow>
            {record.updatedAt}
          </ReactMoment>
        </RecordNavigatorInfoItem>
        <RecordNavigatorButtonItem
          disabled={pristine || submitting || invalid}
          type="submit"
          title="save"
        />
      </RecordNavigator>
    );
  }

  renderRecordNavigatorLinkItems(groups) {
    return _.map(groups, group => {
      return <RecordNavigatorLinkItem key={group.name} id={group._id} name={group.name} />;
    });
  }

  render() {
    const {
      handleSubmit,
      record,
      profile,
      pristine,
      submitting,
      invalid,
      onUpdate,
      customFields,
      view
    } = this.props;

    if (!record || !profile || !profile.tenant || !view) {
      return <div>Loading...</div>;
    }

    this.customFields = _.mapKeys(customFields, '_id');

    return (
      <form action="" className="row" onSubmit={handleSubmit(onUpdate.bind(this))}>
        <div className="d-block d-sm-none col-12">
          <button
            className="btn btn-primary btn-block mt-2"
            disabled={pristine || submitting || invalid}
            type="submit"
          >
            Save
          </button>
        </div>
        <div className="d-none d-sm-block col-sm-2 sidebar">{this.renderNavigator()}</div>
        <div className="d-none d-sm-block col-sm-2">
          <span />
        </div>
        <div className="col-sm-10 col-12 ">{this.renderGroups()}</div>
      </form>
    );
  }
}

function mapStateToProps({ auth }, ownProps) {
  return {
    form: ownProps.formName,
    validate: ownProps.validate,
    profile: auth.profile
  };
}

PrimaryRecord = reduxForm({})(PrimaryRecord);

// You have to connect() to any reducers that you wish to connect to yourself
PrimaryRecord = connect(
  mapStateToProps,
  {} // bind account loading action creator
)(PrimaryRecord);

export default PrimaryRecord;
