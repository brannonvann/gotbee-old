import React from 'react';
import moment from 'moment';
import bytes from 'bytes';

const FileExplorerFile = props => {
  const { name, size, createdAt, _id, openFileHandler, deleteFileHandler } = props;

  return (
    <tr>
      <td>
        <button
          className="btn btn-link text-left white-space-normal"
          type="button"
          onClick={() => {
            openFileHandler(_id);
          }}
        >
          {name}
        </button>
      </td>
      <td className="align-middle">{bytes(size, { decimalPlaces: 0, unitSeparator: ' ' })}</td>
      <td className="align-middle">{moment(createdAt).fromNow()}</td>
      <td>
        <button
          className="btn"
          type="button"
          onClick={() => {
            if (window.confirm(`Are you sure you wish to delete ${name}?`)) deleteFileHandler(_id);
          }}
        >
          <span className="oi oi-trash" />
        </button>
      </td>
    </tr>
  );
};

export default FileExplorerFile;
