import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deleteAlert } from '../actions';

class Alert extends Component {
  constructor(props) {
    super(props);

    this.handleOnClick = this.handleOnClick.bind(this);

    setTimeout(() => {
      this.props.deleteAlert(this.props.alert);
    }, 5000);
  }

  handleOnClick() {
    this.props.deleteAlert(this.props.alert);
  }

  render() {
    const { error, info, success, details } = this.props.alert;
    let className = '';

    if (error) {
      className = 'alert-danger';
    } else if (info) {
      className = 'alert-info';
    } else if (success) {
      className = 'alert-success';
    }

    return (
      <div
        className={'gb-alert alert text-center  alert-dismissible fade show ' + className}
        role="alert"
      >
        {error || info || success}
        <div className="text-muted">{details}</div>
        <button
          type="button"
          className="close"
          data-dismiss="alert"
          aria-label="Close"
          onClick={this.handleOnClick}
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    );
  }
}

export default connect(
  null,
  { deleteAlert }
)(Alert);
