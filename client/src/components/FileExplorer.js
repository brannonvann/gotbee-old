import _ from 'lodash';
import React, { Component } from 'react';

import FileExplorerFile from './FileExplorerFile';

class FileExplorer extends Component {
  constructor(props) {
    super(props);

    this.fileInputOnChangeHandler = this.fileInputOnChangeHandler.bind(this);
    this.deleteFileHandler = this.deleteFileHandler.bind(this);
    this.openFileHandler = this.openFileHandler.bind(this);
  }

  fileInputOnChangeHandler() {
    const fileInput = this.refs.fileInput;
    const { files } = fileInput;
    const file = files[0];
    if (file == null) {
      return alert('No file selected.');
    }

    const { name } = file;

    if (this.props.uploadFileHandler) {
      const matchingFile = this.props.files.find(function(element) {
        return element.name === name;
      });

      if (!matchingFile) {
        this.props.uploadFileHandler(file);
      } else if (
        matchingFile &&
        window.confirm(`The file ${name} already exists. Replace the existing file?`)
      ) {
        this.props.uploadFileHandler(file);
      }

      this.refs.fileInput.value = null;
    }
  }
  openFileHandler(_id) {
    if (this.props.openFileHandler) {
      this.props.openFileHandler(_id);
    }
  }
  deleteFileHandler(_id) {
    if (this.props.deleteFileHandler) {
      this.props.deleteFileHandler(_id);
    }
  }

  renderFiles(files) {
    return _.map(files, file => (
      <FileExplorerFile
        key={file._id}
        {...file}
        openFileHandler={this.openFileHandler}
        deleteFileHandler={this.deleteFileHandler}
      />
    ));
  }

  render() {
    const { files } = this.props;
    return (
      <div className="form-group">
        <label>Files</label>
        <div className="form-control file-tool">
          <div className="pb-2">
            <input
              type="file"
              className="d-none"
              id="fileInput"
              ref="fileInput"
              onChange={this.fileInputOnChangeHandler}
            />
            <button
              className="btn text-dark"
              type="button"
              onClick={() => this.refs.fileInput.click()}
              title="Upload file"
            >
              <span className="oi oi-plus" />
            </button>
          </div>

          <div className="file-tool-table-wrapper">
            <table className="table table-sm file-tool-table">
              <thead>
                <tr>
                  <th scope="col">File</th>
                  <th scope="col">Size</th>
                  <th scope="col">Added</th>
                  <th scope="col" />
                </tr>
              </thead>
              <tbody>{this.renderFiles(files)}</tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default FileExplorer;
