import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import { loginLocal } from '../actions';
import InputField from './inputs/InputField';

class loginLocalForm extends Component {
  renderField(field) {
    return <InputField field={field} />;
  }

  onSubmit(values) {
    this.props.loginLocal(values);
  }

  render() {
    const { handleSubmit, profile } = this.props;

    if (profile) {
      return <Redirect to="/people" />;
    }

    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
          <h5>Login to gotbee</h5>
          <Field
            kind="email"
            label="Email"
            placeholder="Email"
            name="email"
            component={this.renderField}
          />
          <Field
            kind="password"
            label="Password"
            placeholder="Password"
            name="password"
            component={this.renderField}
          />
          <button className="btn btn-primary" type="submit">
            Log In
          </button>
          {this.props.children}
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (!values.email) {
    errors.email = 'Enter an email address!';
  }

  if (!values.password) {
    errors.password = 'Enter a password!';
  }
  return errors;
}

function mapStateToProps({ auth }) {
  return { profile: auth.profile };
}

export default reduxForm({
  validate,
  form: 'LoginLocalForm'
})(
  connect(
    mapStateToProps,
    { loginLocal }
  )(loginLocalForm)
);
