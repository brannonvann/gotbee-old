import React, { Component } from 'react';

class FieldColumn extends Component {
  render() {
    const { width } = this.props;
    const className = 'col-12' + (width && width === 'half' ? ' col-md-6' : '');
    return <div className={className}>{this.props.children}</div>;
  }
}

export default FieldColumn;
