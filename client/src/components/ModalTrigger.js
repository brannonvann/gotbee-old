import React, { Component } from 'react';

const ModalTrigger = ({ text }) => <button type="button">{text}</button>;

class Modal extends Component {
  render() {
    const { triggerText } = this.props;
    return <ModalTrigger text={triggerText} />;
  }
}

export default Modal;
