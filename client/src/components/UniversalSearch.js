import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchSearch, setSearchTerm, searchAllClear } from '../actions';
import UniversalSearchList from './UniversalSearchList';

class UniversalSearch extends Component {
  handleOnClick(event) {
    //event.preventDefault();
    //console.log(event.target.href);
    this.props.people = [];
  }

  render() {
    const fetchSearch = _.debounce(term => {
      this.props.fetchSearch(term);
    }, 300);

    return (
      <div className="u-search-wrapper">
        <div className="input-group">
          <input
            className="form-control u-search-input"
            type="text"
            value={this.props.term || ''}
            placeholder="Search"
            list="u-search-list"
            onChange={event => {
              this.props.setSearchTerm(event.target.value);
              fetchSearch(event.target.value);
            }}
          />
          <div className="input-group-append">
            <button
              onClick={() => this.props.searchAllClear()}
              className="btn btn-outline-secondary"
              type="button"
            >
              x
            </button>
          </div>
        </div>

        <UniversalSearchList
          handleOnClick={this.handleOnClick}
          people={this.props.people}
        />
      </div>
    );
  }
}

function mapStateToProps({ search: { term, people } }) {
  //console.log('term initial', term);
  return {
    people,
    term
  };
}

export default connect(
  mapStateToProps,
  { setSearchTerm, fetchSearch, searchAllClear }
)(UniversalSearch);
