import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import UniversalSearchListItem from './UniversalSearchListItem';

class UniversalSearchList extends Component {
  renderResults(people) {
    return _.map(people, person => {
      return (
        <UniversalSearchListItem
          key={person._id}
          name={person.name}
          type="people"
          id={person._id}
          handleOnClick={this.props.handleOnClick}
        />
      );
    });
  }

  render() {
    if (!this.props.people) {
      return null;
    }

    return (
      <div className="list-group position-absolute" style={{ zIndex: 1000 }}>
        {this.renderResults(this.props.people)}
      </div>
    );
  }
}

export default connect(null)(UniversalSearchList);
