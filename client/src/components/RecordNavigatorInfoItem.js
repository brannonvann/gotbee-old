import React from 'react';
import RecordNavigatorShell from './RecordNavigatorShell';

const RecordNavigatorInfoItem = ({ children, title }) => {
  return (
    <RecordNavigatorShell>
      <p title={title}>
        <small className="text-muted">{children}</small>
      </p>
    </RecordNavigatorShell>
  );
};

export default RecordNavigatorInfoItem;
