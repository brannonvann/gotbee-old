import React, { Component } from 'react';

class SmallFormContainer extends Component {
  render() {
    const { children } = this.props;

    return (
      <div className="container">
        <div className="row justify-content-md-center">
          <div className="col-md-6">{children}</div>
        </div>
      </div>
    );
  }
}

export default SmallFormContainer;
