import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { forgotPasswordReset } from '../actions';
import InputField from './inputs/InputField';
import PasswordRules from '../components/PasswordRules';

class ForgotPasswordResetForm extends Component {
  constructor(props) {
    super(props);

    this.state = { success: false };
  }

  renderField(field) {
    return <InputField field={field} />;
  }

  onSubmit(values) {
    this.props.forgotPasswordReset(this.props.id, values, () => {
      this.setState({ success: true });
    });
  }

  render() {
    const { handleSubmit, pristine, submitting, invalid } = this.props;
    const { success } = this.state;

    if (success) return <Redirect to="/login" />;

    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
          <h5>Please enter your email address and new password below</h5>
          <Field label="Email" name="email" kind="email" component={this.renderField} />
          <Field label="Password" kind="password" name="password" component={this.renderField} />
          <Field
            label="Confirm Password"
            kind="password"
            name="confirm-password"
            component={this.renderField}
          />
          <button
            className="btn btn-primary"
            type="submit"
            disabled={pristine || submitting || invalid}
          >
            Update Password
          </button>
          {this.props.children}
        </form>
        <PasswordRules />
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (!values.email) {
    errors.email = 'Enter your email address!';
  }

  if (!values.password) {
    errors.password = 'Enter a password!';
  }

  if (values.password !== values['confirm-password']) {
    errors['confirm-password'] = 'Passwords do not match!';
  }

  return errors;
}

export default reduxForm({
  validate,
  form: 'ForgotPasswordResetForm' //think of as the name of the form. needs to be unique!
})(
  connect(
    null,
    { forgotPasswordReset }
  )(ForgotPasswordResetForm)
);
