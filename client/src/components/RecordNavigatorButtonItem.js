import React from 'react';
import RecordNavigatorShell from './RecordNavigatorShell';

const RecordNavigatorButtonItem = ({ disabled, title, type }) => {
  return (
    <RecordNavigatorShell>
      <button
        className="btn btn-primary btn-block"
        type={type}
        disabled={disabled}
      >
        {title}
      </button>
    </RecordNavigatorShell>
  );
};

export default RecordNavigatorButtonItem;
