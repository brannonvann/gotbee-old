var expect = require('chai').expect
    , auth = require("../modules/auth.js")

describe('modules/auth.js', function () {

    describe('HashPersonPassword', function () {

        it('should exist', function () {
            expect(auth.HashPersonPassword).to.exist;
        });

        it('should be a function', function () {
            expect(auth.HashPersonPassword).to.be.a('function');
        });

        it('should return password as first level property in a json object when given an object with first level password.', function (done) {
            auth.HashPersonPassword({ password: "BLAHHH" }, function (err, result) {
                expect(result).to.not.have.property('password');
                done();
            })
        });

        it('should return object with private propertyin a json object when given an object with first level password.', function (done) {
            auth.HashPersonPassword({ password: "BLAHHH" }, function (err, result) {
                expect(result).to.have.property('private');
                done();
            })
        });

        it('should return object with private.password property in a json object when given an object with first level password.', function (done) {
            auth.HashPersonPassword({ password: "BLAHHH" }, function (err, result) {
                expect(result.private).to.have.property('password');
                done();
            })
        });

        it('should return object with private.password property <> to the password... in a json object when given an object with first level password.', function (done) {
            var password = "BLAHHH";
            auth.HashPersonPassword({ password: password }, function (err, result) {
                expect(result.private.password).to.not.equal(password);
                done();
            })
        });

        //
        it('should return password as first level property in a json object when given an object with private.password.', function (done) {
            auth.HashPersonPassword({ private: { password: "BLAHHH" } }, function (err, result) {
                expect(result).to.not.have.property('password');
                done();
            })
        });

        it('should return object with private propertyin a json object when given an object with private.password.', function (done) {
            auth.HashPersonPassword({ private: { password: "BLAHHH" } }, function (err, result) {
                expect(result).to.have.property('private');
                done();
            })
        });

        it('should return object with private.password property in a json object when given an object with private.password.', function (done) {
            auth.HashPersonPassword({ private: { password: "BLAHHH" } }, function (err, result) {
                expect(result.private).to.have.property('password');
                done();
            })
        });

        it('should return object with private.password property <> to the password... in a json object when given an object with private.password.', function (done) {
            var password = "BLAHHH";
            auth.HashPersonPassword({ private: { password: password } }, function (err, result) {
                expect(result.private.password).to.not.equal(password);
                done();
            })
        });

        it('should return same object when .password and .private is not included', function (done) {
            var person = { "firstName": "test", "lastName": "machine" };
            auth.HashPersonPassword(person, function (err, result) {
                expect(result).to.equal(person);
                done();
            })
        });

        it('should return same object when .password is not included and .private is included but no .private.password', function (done) {
            var person = { "firstName": "test", "lastName": "machine", private: { "loginAttempts": 3 } };
            auth.HashPersonPassword(person, function (err, result) {
                expect(result).to.equal(person);
                done();
            })
        });

    });


})