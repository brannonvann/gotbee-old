var expect = require('chai').expect
, foo = 'bar'
, beverages = { tea: [ 'chai', 'matcha', 'oolong' ] };


describe('Login', function() {  
    describe('#foo', function() {
        it('should be a string', function() {
            expect(foo).to.be.a('string');
        });
    });

    describe('#foo', function() {
        it('should be equal to "bar"', function() {
            expect(foo).to.equal('bar');
        });
    });

    describe('#foo', function() {
        it('should have a length of 3', function() {
            expect(foo).to.have.lengthOf(3);
        });
    });

    describe('#foo', function() {
        it('should have a length of 3', function() {
            expect(beverages).to.have.property('tea').with.lengthOf(3);
        });
    });
});
