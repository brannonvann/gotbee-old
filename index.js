require('dotenv').config({ path: './config/.env' });
const port = process.env.PORT || 8080;
const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const app = express();
const auth = require('./modules/auth');
const bodyParser = require('body-parser');
const passport = require('passport');

//load models
require('./models/tenant');
require('./models/person');
require('./models/project');
require('./models/authentication');
require('./models/temporaryAuthentication');

//load modules
require('./modules/passport'); //

//Databse Setup
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  //console.log('database connection successful');
});

//middleware injection

app.use(function(req, res, next) {
  console.info('[INFO] ' + req.method + ' - ' + req.originalUrl);
  next();
});

//force https in production middlewear
app.use(function(req, res, next) {
  if (process.env.NODE_ENV === 'production' && req.headers['x-forwarded-proto'] !== 'https') {
    return res.redirect(`https://${req.hostname}${req.url}`);
  }
  next();
});

//use parsers to get data from incoming requests.
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({ extended: false }));

//cookie & session
app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [process.env.COOKIE_KEY]
  })
);

app.use(passport.initialize());
app.use(passport.session());

//api route configuration
app.use('/api/1/tenants', auth.isLoggedIn, require('./routes/api_tenants'));
app.use('/api/1/people', auth.isLoggedIn, require('./routes/api_people'));
app.use('/api/1/projects', auth.isLoggedIn, require('./routes/api_projects'));
app.use('/api/1/search', auth.isLoggedIn, require('./routes/api_search'));
app.use('/api/1/auth', require('./routes/api_auth'));

//block robots using robot.txt
app.get('/robots.txt', function(req, res) {
  res.type('text/plain');
  res.send('User-agent: *\nDisallow: /');
});

//serve client resources in production
if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'));

  const path = require('path');
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

app.listen(port);
